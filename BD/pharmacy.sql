-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-12-2020 a las 22:25:19
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ventasdb2`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `add_detalle_temp` (`codigo` INT, `cantidad` INT, `token_user` VARCHAR(50))  BEGIN

DECLARE precio_actual decimal(10,2);

SELECT unit_sale_price INTO precio_actual FROM frm_product WHERE id_product = codigo;

INSERT INTO detalle_temp(token_user, codproducto, cantidad, precio_venta) VALUES (token_user, codigo, cantidad, precio_actual);
SELECT tmp.correlativo, tmp.codproducto, p.name, tmp.cantidad, tmp.precio_venta FROM detalle_temp tmp INNER JOIN frm_product p ON tmp.codproducto = p.id_product WHERE tmp.token_user = token_user;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `data` ()  BEGIN
DECLARE usuarios int;
DECLARE clientes int;
DECLARE proveedores int;
DECLARE productos int;
DECLARE ventas int;
SELECT COUNT(*) INTO usuarios FROM frm_user;
SELECT COUNT(*) INTO clientes FROM frm_client;
SELECT COUNT(*) INTO proveedores FROM frm_provider;
SELECT COUNT(*) INTO productos FROM frm_product;
SELECT COUNT(*) INTO ventas FROM frm_sales WHERE sale_date = DATE_FORMAT(now(),'%Y/%m/%d');

SELECT usuarios, clientes, proveedores, productos, ventas;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `del_detalle_temp` (`id_detalle` INT, `token` VARCHAR(50))  BEGIN
DELETE FROM detalle_temp WHERE correlativo = id_detalle;
SELECT tmp.correlativo, tmp.codproducto, p.name, tmp.cantidad, tmp.precio_venta FROM detalle_temp tmp INNER JOIN frm_product p ON tmp.codproducto = p.id_product WHERE tmp.token_user = token_user;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `frm_box_data` (IN `current_da` DATE)  BEGIN

DECLARE efectivo DECIMAL(10,2);
DECLARE tarjeta DECIMAL(10,2);
DECLARE credito DECIMAL(10,2);
DECLARE otros DECIMAL(10,2);
DECLARE saldo_inicial DECIMAL(10,2);
DECLARE Saldo_final DECIMAL(10,2);
DECLARE sub_total DECIMAL(10,2);
DECLARE fecha date;

set fecha = current_da;

select sum(initial_balance) into saldo_inicial from frm_box where open_date = current_da;
select sum(final_balance) into Saldo_final from frm_box where open_date = current_da;

select sum(total_sale) into efectivo from frm_sales where sale_date=current_da and id_type_pyment=1;
select sum(total_sale) into tarjeta from frm_sales where sale_date=current_da and id_type_pyment=2;
select sum(total_sale) into credito from frm_sales where sale_date=current_da and id_type_pyment=3;
select sum(total_sale) into otros from frm_sales where sale_date=current_da and id_type_pyment=4;


select saldo_inicial, efectivo, tarjeta, credito, otros, sum(ifnull( efectivo, 0 )+ifnull( tarjeta, 0 )+ifnull( credito, 0 )+ifnull( otros, 0 )) as total, fecha,Saldo_final;




END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `frm_sp_box` (IN `p_option` CHAR(100), IN `p_id_user` INT(11), IN `p_initial_balance` FLOAT(10,2), IN `p_final_balance` FLOAT(10,2))  BEGIN
	DECLARE boxid INT;
    
	if p_option = 'openBox' then 
		INSERT INTO frm_box(id_user, open_date, open_time,initial_balance, final_balance, is_open) 
						values(p_id_user,CURDATE(),curtime(),p_initial_balance,p_initial_balance,1);
    end if;
    
    if p_option = 'closeBox' then
		SELECT id_box INTO boxid FROM frm_box WHERE is_open = 1;
		UPDATE frm_box SET is_open = 0, 
                            end_date = curdate(),
                            end_time= curtime()
                            WHERE id_box = boxid;    
    end if;
    



END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `frm_sp_product` (IN `p_option` CHAR(100), IN `p_id_product` INT(11), IN `p_id_provider` INT(11), IN `p_id_user` INT(11), IN `p_bar_cod` CHAR(20), IN `p_name` VARCHAR(150), IN `p_brand` VARCHAR(45), IN `p_presentation` VARCHAR(45), IN `p_purchase_price` FLOAT, IN `p_id_sales_unit` INT(11), IN `p_unit_sale_price` FLOAT, IN `p_expiration` DATE, IN `p_stock` INT, IN `p_activo` INT)  BEGIN

	if p_option = 'saveProduct' then 
		INSERT INTO frm_product(id_provider, id_user, bar_cod,name, brand, presentation, purchase_price, id_sales_unit, unit_sale_price, expiration, stock, created_at, active) 
						values(p_id_provider,p_id_user,p_bar_cod, p_name,p_brand,p_presentation,p_purchase_price,p_id_sales_unit,p_unit_sale_price,p_expiration, p_stock, Date_format(now(),'%Y/%m/%d'), p_activo);
    end if;
    
    if p_option = 'editProduct' then
		UPDATE frm_product SET id_provider = p_id_provider, 
							id_user=p_id_user, 
							bar_cod = p_bar_cod, 
							name = p_name,
							brand=p_brand,
                            presentation=p_presentation,
                            purchase_price=p_purchase_price,
                            id_sales_unit=p_id_sales_unit,
                            unit_sale_price=p_unit_sale_price,
                            expiration=p_expiration,
                            stock=p_stock
                            WHERE id_product = p_id_product;    
    end if;
    
    if p_option = 'getProducts' then
		select p.id_product,p.bar_cod, p.name, p.brand, p.presentation, s.name, p.stock, p.unit_sale_price, p.expiration, pr.name as Proveedor from frm_product p
		left join frm_sales_unit s on p.id_sales_unit=s.id_sales_unit
        left join frm_provider pr on p.id_provider=pr.id_provider;
    
    end if;


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `frm_sp_provider` (IN `p_option` CHAR(100), IN `p_id_provider` INT(11), IN `p_name` VARCHAR(45), IN `p_address` VARCHAR(50), IN `p_ruc` CHAR(15), IN `p_phone` CHAR(15), IN `p_person_contact` VARCHAR(45), IN `p_activo` INT)  BEGIN

	if p_option = 'saveProvider' then 
		INSERT INTO frm_provider(name,address,ruc,phone,person_contact,active) values (p_name, p_address,p_ruc, p_phone,p_person_contact, p_activo);
    end if;
    
    if p_option = 'editProvider' then
		UPDATE frm_provider SET name = p_name, 
							address=p_address, 
							ruc = p_ruc, 
							phone = p_phone,
							person_contact=p_person_contact WHERE id_provider = p_id_provider;    
    end if;


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `frm_sp_user` (IN `p_option` CHAR(100), IN `p_iduser` INT, IN `p_idrol` INT, IN `p_name` VARCHAR(45), IN `p_surname` VARCHAR(50), IN `p_email` VARCHAR(45), IN `p_password` VARCHAR(150), IN `p_activo` INT)  BEGIN

	if p_option = 'saveUser' then 
		INSERT INTO frm_user(idapp_rols,name,surname,email,password,created_at,active) values (p_idrol, p_name, p_surname, p_email, p_password, Date_format(now(),'%Y-%m-%d %h:%i:%s %p'), p_activo);
    end if;
    
    if p_option = 'editUser' then
		UPDATE frm_user SET idapp_rols = p_idrol, name = p_name, surname=p_surname, email = p_email, update_at = Date_format(now(),'%Y-%m-%d %h:%i:%s %p') WHERE id_user = p_iduser;    
    end if;


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `procesar_venta` (IN `cod_usuario` INT, IN `cod_cliente` INT, IN `cod_type_sales` INT, IN `cod_type_pyment` INT, IN `token` VARCHAR(50))  BEGIN
DECLARE factura INT;
DECLARE registros INT;
DECLARE total DECIMAL(10,2);
DECLARE nueva_existencia int;
DECLARE existencia_actual int;

DECLARE tmp_cod_producto int;
DECLARE tmp_cant_producto int;
DECLARE a int;
SET a = 1;

### send data to update stock
CREATE TEMPORARY TABLE tbl_tmp_tokenuser(
	id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    cod_prod char(20),
    cant_prod int);
    
## length 
SET registros = (SELECT COUNT(*) FROM detalle_temp WHERE token_user = token);
IF registros > 0 THEN

	INSERT INTO tbl_tmp_tokenuser(cod_prod, cant_prod) SELECT codproducto, cantidad FROM detalle_temp WHERE token_user = token;
	INSERT INTO frm_sales (id_type_sales,id_type_pyment,id_user,sale_date,time) VALUES (cod_type_sales,cod_type_pyment,cod_usuario,DATE_FORMAT(now(),'%Y/%m/%d'),DATE_FORMAT(NOW( ), "%H:%i:%S" ));
    
	SET factura = LAST_INSERT_ID();

	INSERT INTO frm_sales_detail(id_sales,cod_product,quantity,sale_price) SELECT (factura) AS id_sales, codproducto, cantidad,precio_venta FROM detalle_temp WHERE token_user = token;
	#### update stock
	WHILE a <= registros DO
		SELECT cod_prod, cant_prod INTO tmp_cod_producto,tmp_cant_producto FROM tbl_tmp_tokenuser WHERE id = a;
		SELECT stock INTO existencia_actual FROM frm_product WHERE id_product = tmp_cod_producto;
		SET nueva_existencia = existencia_actual - tmp_cant_producto;
		UPDATE frm_product SET stock = nueva_existencia WHERE id_product = tmp_cod_producto;
		SET a=a+1;
	END WHILE;

	SET total = (SELECT SUM(cantidad * precio_venta) FROM detalle_temp WHERE token_user = token);
	UPDATE frm_sales SET total_sale = total WHERE id_sales = factura;
	DELETE FROM detalle_temp WHERE token_user = token;
	DROP TABLE tbl_tmp_tokenuser;
	SELECT * FROM frm_sales WHERE id_sales = factura;
ELSE
	SELECT 0;
END IF;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_temp`
--

CREATE TABLE `detalle_temp` (
  `correlativo` int(11) NOT NULL,
  `token_user` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `codproducto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio_venta` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fmr_pharmacy`
--

CREATE TABLE `fmr_pharmacy` (
  `id_pharmacy` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `phone` char(15) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `ruc` varchar(45) DEFAULT NULL,
  `address` varchar(65) DEFAULT NULL,
  `representative` varchar(100) DEFAULT NULL,
  `igv` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fmr_pharmacy`
--

INSERT INTO `fmr_pharmacy` (`id_pharmacy`, `name`, `descripcion`, `phone`, `email`, `ruc`, `address`, `representative`, `igv`) VALUES
(1, 'Mifarma S.A.A', 'Botica que vende medicamentos', '9235698471', 'vidafarma@gmail.com', '02145236871', 'Av. Bolivar 23011', 'David Pichihua', '1.18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `frm_box`
--

CREATE TABLE `frm_box` (
  `id_box` int(11) NOT NULL,
  `id_user` int(8) NOT NULL,
  `open_date` date DEFAULT NULL,
  `open_time` time DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `initial_balance` float(10,2) NOT NULL,
  `final_balance` float(10,2) DEFAULT NULL,
  `is_open` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `frm_client`
--

CREATE TABLE `frm_client` (
  `idcliente` int(11) NOT NULL,
  `dni` int(8) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` int(15) DEFAULT NULL,
  `direccion` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `usuario_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `frm_client`
--

INSERT INTO `frm_client` (`idcliente`, `dni`, `nombre`, `telefono`, `direccion`, `usuario_id`) VALUES
(6, 71347260, 'rodil0', 485123120, 'dwq0', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `frm_config`
--

CREATE TABLE `frm_config` (
  `id_config` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `icon` varchar(1600) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `frm_module`
--

CREATE TABLE `frm_module` (
  `id_module` int(11) NOT NULL,
  `order` varchar(45) DEFAULT NULL,
  `name` varchar(45) NOT NULL,
  `title` varchar(45) NOT NULL,
  `description` varchar(65) DEFAULT NULL,
  `icon` varchar(1600) DEFAULT NULL,
  `active` tinyint(4) NOT NULL,
  `labelledby` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `frm_module`
--

INSERT INTO `frm_module` (`id_module`, `order`, `name`, `title`, `description`, `icon`, `active`, `labelledby`) VALUES
(1, '1', 'collapseTwo', 'Ventas', '--', 'fas fa-money-check-alt', 1, 'headingTwo'),
(2, '2', 'collapseUtilities', 'Productos', '--', 'fa fa-suitcase', 1, 'headingUtilities'),
(3, '3', 'collapseClientes', 'Clientes', '--', 'fas fa-users', 1, 'headingUtilities'),
(4, '4', 'collapseProveedor', 'Proveedor', '--', 'fas fa-hospital', 1, 'headingUtilities'),
(5, '5', 'collapseUsuarios', 'Usuarios', '--', 'fas fa-user', 1, 'headingUtilities'),
(6, '6', 'collapsePermisos', 'Permisos', '--', 'fas fa-shield-alt', 1, 'headingUtilities'),
(7, '7', 'collapseConfig', 'Ajustes', '--', 'fas fa-cog', 1, 'headingUtilities');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `frm_module_main_transaction`
--

CREATE TABLE `frm_module_main_transaction` (
  `id_module_main` int(11) NOT NULL,
  `id_rols` int(11) NOT NULL,
  `id_module_type` int(11) NOT NULL,
  `id_module` int(11) NOT NULL,
  `order` varchar(2) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `frm_module_main_transaction`
--

INSERT INTO `frm_module_main_transaction` (`id_module_main`, `id_rols`, `id_module_type`, `id_module`, `order`, `active`) VALUES
(1, 1, 1, 1, '1', 1),
(2, 1, 1, 2, '2', 1),
(3, 1, 1, 3, '3', 1),
(4, 1, 1, 4, '4', 1),
(5, 1, 1, 5, '5', 1),
(6, 1, 1, 6, '6', 1),
(7, 1, 1, 7, '7', 1),
(8, 4, 1, 1, NULL, 1),
(9, 4, 1, 2, NULL, 1),
(10, 4, 1, 3, NULL, 1),
(11, 4, 1, 4, NULL, 1),
(12, 4, 1, 5, NULL, 0),
(13, 4, 1, 6, NULL, 0),
(14, 4, 1, 7, NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `frm_module_type`
--

CREATE TABLE `frm_module_type` (
  `idmodule_type` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(45) NOT NULL,
  `isActive` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `frm_module_type`
--

INSERT INTO `frm_module_type` (`idmodule_type`, `name`, `description`, `isActive`) VALUES
(1, 'Main Menu', 'lista de Modulos', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `frm_product`
--

CREATE TABLE `frm_product` (
  `id_product` int(11) NOT NULL,
  `id_provider` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `bar_cod` char(20) DEFAULT NULL,
  `name` varchar(150) NOT NULL,
  `brand` varchar(45) DEFAULT NULL,
  `presentation` varchar(45) DEFAULT NULL,
  `purchase_price` float(10,2) DEFAULT NULL,
  `id_sales_unit` int(11) NOT NULL,
  `unit_sale_price` float(10,2) NOT NULL,
  `expiration` date DEFAULT NULL,
  `stock` int(11) NOT NULL,
  `created_at` date DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `frm_product`
--

INSERT INTO `frm_product` (`id_product`, `id_provider`, `id_user`, `bar_cod`, `name`, `brand`, `presentation`, `purchase_price`, `id_sales_unit`, `unit_sale_price`, `expiration`, `stock`, `created_at`, `active`) VALUES
(4, 1, 6, '02365236', 'ASPIRINA 100 100 mg ', 'BAYER', 'EMBACE L', 12.00, 1, 0.50, '2021-01-01', 20, '0000-00-00', 1),
(7, 2, 6, '02553543', 'ASPIRINA 500 mg ', 'LABOT', 'Tableta', 28.80, 1, 1.20, '2021-01-05', 15, '2020-12-07', 1),
(8, 1, 6, '021452565561', 'Panadol 500G1', 'BAYER', 'Tableta', 30.50, 1, 1.50, '2024-10-29', 50, '2020-12-15', 1),
(9, 2, 6, '2451114225', 'AMOXICILINA 500 mg', 'LABOT', 'Tableta', 72.00, 1, 0.50, '2028-01-11', 107, '2020-12-15', 1),
(10, 5, 6, '023696369', 'Agua sin Gas CIELO 1LT', 'Cielo', '1LT', 48.00, 1, 2.00, '2024-06-12', 7, '2020-12-19', 1),
(11, 1, 6, '252536365', 'CREMA DEL DR. ZAIDMAN Crema', 'MD', '500G', 144.00, 1, 23.00, '0000-00-00', 3, '2020-12-19', 1),
(12, 1, 6, '02145245', 'DOLORAL 200 mg', 'VAYER', '200 mg', 24.00, 1, 1.00, '2025-06-10', 24, '2020-12-19', 1),
(13, 1, 6, '023565612', 'Paracetamol 200MG', 'vayer', '500G', 35.00, 1, 1.00, '2021-01-04', 28, '2020-12-19', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `frm_provider`
--

CREATE TABLE `frm_provider` (
  `id_provider` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `address` varchar(50) DEFAULT NULL,
  `ruc` char(15) DEFAULT NULL,
  `phone` char(15) DEFAULT NULL,
  `person_contact` varchar(45) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `frm_provider`
--

INSERT INTO `frm_provider` (`id_provider`, `name`, `address`, `ruc`, `phone`, `person_contact`, `active`) VALUES
(1, 'Bayer S.A.', ' Calle Las Begonias 475, Cercado de Lima', '023659856', '920325639', ' Calle Las Begonias 475, Cercado de Lima', 1),
(2, 'Abbott Laboratories del Perú S.A.1', 'Av. San fernando', '023225985601', '12212101', 'Jorge Diaz', 1),
(5, 'Medifarma S.A 0', 'Av. bbbbbb', '021315453120', '02315650', 'david ', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `frm_rols`
--

CREATE TABLE `frm_rols` (
  `id_rols` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `active` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `frm_rols`
--

INSERT INTO `frm_rols` (`id_rols`, `name`, `description`, `active`) VALUES
(1, 'Administrador', 'Tiene acceso a todos los módulos ', 1),
(4, 'Vendedor', 'Encargado de vender ', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `frm_rol_permission`
--

CREATE TABLE `frm_rol_permission` (
  `id_permission` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `frm_sales`
--

CREATE TABLE `frm_sales` (
  `id_sales` int(11) NOT NULL,
  `id_type_sales` int(11) NOT NULL,
  `id_type_pyment` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `sale_date` date DEFAULT NULL,
  `time` varchar(25) DEFAULT NULL,
  `total_sale` float(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `frm_sales_detail`
--

CREATE TABLE `frm_sales_detail` (
  `id_sales_detail` int(11) NOT NULL,
  `id_sales` int(11) NOT NULL,
  `cod_product` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `discount` float DEFAULT '0',
  `sale_price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `frm_sales_unit`
--

CREATE TABLE `frm_sales_unit` (
  `id_sales_unit` int(11) NOT NULL,
  `name` varchar(25) NOT NULL,
  `description` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `frm_sales_unit`
--

INSERT INTO `frm_sales_unit` (`id_sales_unit`, `name`, `description`) VALUES
(1, 'UND', 'tipo de venta unitario'),
(2, 'ML', '--'),
(3, 'CAJA', '--'),
(4, 'MG', '--'),
(5, 'GR', '--');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `frm_sub_module`
--

CREATE TABLE `frm_sub_module` (
  `id_submodule` int(11) NOT NULL,
  `id_module` int(11) NOT NULL,
  `order` varchar(45) DEFAULT NULL,
  `name` varchar(45) NOT NULL,
  `titlle` varchar(45) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `icon` varchar(2000) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `frm_sub_module`
--

INSERT INTO `frm_sub_module` (`id_submodule`, `id_module`, `order`, `name`, `titlle`, `description`, `icon`, `active`) VALUES
(1, 1, '1', 'collapseTwo', 'Nueva venta', 'nueva_venta.php', '--', 1),
(2, 1, '2', 'collapseTwo', 'Ventas', 'ventas.php', '--', 1),
(3, 2, '1', 'collapseUtilities', 'Nuevo Producto', 'registro_producto.php', '--', 1),
(4, 2, '2', 'collapseUtilities', 'Productos', 'lista_productos.php', '--', 1),
(5, 3, '1', 'collapseClientes', 'Nuevo Clientes', 'registro_cliente.php', '--', 1),
(6, 3, '2', 'collapseClientes', 'Clientes', 'lista_cliente.php', '--', 1),
(7, 4, '1', 'collapseProveedor', 'Nuevo Proveedor', 'registro_proveedor.php', '--', 1),
(8, 4, '2', 'collapseProveedor', 'Proveedores', 'lista_proveedor.php', '--', 1),
(9, 5, '1', 'collapseUsuarios', 'Nuevo Usuario', 'registro_usuario.php', '--', 1),
(10, 5, '2', 'collapseUsuarios', 'Usuarios', 'lista_usuarios.php', '--', 1),
(11, 6, '1', 'collapsePermisos', 'Agregar Rol', 'add_permissions.php', '--', 1),
(12, 6, '2', 'collapsePermisos', 'Roles', 'list_roles.php', '--', 1),
(13, 2, '3', 'collapseUtilities', 'Por Vencer', 'expire_products.php', '--', 1),
(14, 1, '3', 'collapseTwo', 'Caja', 'open_box.php', '--', 1),
(15, 7, '1', 'collapseAjustes', 'Mi Empresa', 'company.php', '--', 1),
(16, 1, '4', 'collapseTwo', 'Cuadrar Caja', 'sq_box.php', '--', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `frm_type_pyment`
--

CREATE TABLE `frm_type_pyment` (
  `id_type_pyment` int(11) NOT NULL,
  `name` varchar(25) NOT NULL,
  `description` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `frm_type_pyment`
--

INSERT INTO `frm_type_pyment` (`id_type_pyment`, `name`, `description`) VALUES
(1, 'Efectivo', '--'),
(2, 'Tarjeta', '--'),
(3, 'Crédito', '--'),
(4, 'Otros', '--');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `frm_type_sales`
--

CREATE TABLE `frm_type_sales` (
  `id_type_sales` int(11) NOT NULL,
  `name` varchar(25) NOT NULL,
  `description` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `frm_type_sales`
--

INSERT INTO `frm_type_sales` (`id_type_sales`, `name`, `description`) VALUES
(1, 'Boleta', '--'),
(2, 'Factura', '--');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `frm_user`
--

CREATE TABLE `frm_user` (
  `id_user` int(8) NOT NULL,
  `idapp_rols` int(11) NOT NULL,
  `name` varchar(25) NOT NULL,
  `surname` varchar(50) DEFAULT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(150) NOT NULL,
  `created_at` varchar(45) DEFAULT NULL,
  `update_at` varchar(45) DEFAULT NULL,
  `active` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `frm_user`
--

INSERT INTO `frm_user` (`id_user`, `idapp_rols`, `name`, `surname`, `email`, `password`, `created_at`, `update_at`, `active`) VALUES
(6, 1, 'Rodil', 'Pampañaupa Velasque', 'rodil@admin.com', 'YWRtaW4=', '2020-11-27 05:25:46 PM', NULL, 1),
(12, 4, 'david', 'Pichihua Vila', 'david@gmail.com', 'ZGF2aWQxMjM=', '2020-12-21 10:30:42 PM', NULL, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `detalle_temp`
--
ALTER TABLE `detalle_temp`
  ADD PRIMARY KEY (`correlativo`);

--
-- Indices de la tabla `fmr_pharmacy`
--
ALTER TABLE `fmr_pharmacy`
  ADD PRIMARY KEY (`id_pharmacy`);

--
-- Indices de la tabla `frm_box`
--
ALTER TABLE `frm_box`
  ADD PRIMARY KEY (`id_box`),
  ADD KEY `fk_frm_box_frm_user1` (`id_user`);

--
-- Indices de la tabla `frm_client`
--
ALTER TABLE `frm_client`
  ADD PRIMARY KEY (`idcliente`);

--
-- Indices de la tabla `frm_config`
--
ALTER TABLE `frm_config`
  ADD PRIMARY KEY (`id_config`);

--
-- Indices de la tabla `frm_module`
--
ALTER TABLE `frm_module`
  ADD PRIMARY KEY (`id_module`);

--
-- Indices de la tabla `frm_module_main_transaction`
--
ALTER TABLE `frm_module_main_transaction`
  ADD PRIMARY KEY (`id_module_main`),
  ADD KEY `fk_app_module_main_app_module_type1` (`id_module_type`),
  ADD KEY `fk_app_module_main_app_module1` (`id_module`),
  ADD KEY `fk_app_module_main_app_rols1` (`id_rols`);

--
-- Indices de la tabla `frm_module_type`
--
ALTER TABLE `frm_module_type`
  ADD PRIMARY KEY (`idmodule_type`);

--
-- Indices de la tabla `frm_product`
--
ALTER TABLE `frm_product`
  ADD PRIMARY KEY (`id_product`),
  ADD KEY `fk_frm_product_frm_sales_unit1` (`id_sales_unit`),
  ADD KEY `fk_frm_product_frm_provider1` (`id_provider`);

--
-- Indices de la tabla `frm_provider`
--
ALTER TABLE `frm_provider`
  ADD PRIMARY KEY (`id_provider`);

--
-- Indices de la tabla `frm_rols`
--
ALTER TABLE `frm_rols`
  ADD PRIMARY KEY (`id_rols`);

--
-- Indices de la tabla `frm_rol_permission`
--
ALTER TABLE `frm_rol_permission`
  ADD PRIMARY KEY (`id_permission`);

--
-- Indices de la tabla `frm_sales`
--
ALTER TABLE `frm_sales`
  ADD PRIMARY KEY (`id_sales`),
  ADD KEY `fk_frm_sales_frm_type_sales1` (`id_type_sales`),
  ADD KEY `fk_frm_sales_frm_type_pyment1` (`id_type_pyment`);

--
-- Indices de la tabla `frm_sales_detail`
--
ALTER TABLE `frm_sales_detail`
  ADD PRIMARY KEY (`id_sales_detail`),
  ADD KEY `fk_frm_sales_detail_frm_sales` (`id_sales`);

--
-- Indices de la tabla `frm_sales_unit`
--
ALTER TABLE `frm_sales_unit`
  ADD PRIMARY KEY (`id_sales_unit`);

--
-- Indices de la tabla `frm_sub_module`
--
ALTER TABLE `frm_sub_module`
  ADD PRIMARY KEY (`id_submodule`),
  ADD KEY `fk_app_sub_module_app_module1` (`id_module`);

--
-- Indices de la tabla `frm_type_pyment`
--
ALTER TABLE `frm_type_pyment`
  ADD PRIMARY KEY (`id_type_pyment`);

--
-- Indices de la tabla `frm_type_sales`
--
ALTER TABLE `frm_type_sales`
  ADD PRIMARY KEY (`id_type_sales`);

--
-- Indices de la tabla `frm_user`
--
ALTER TABLE `frm_user`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `fk_app_user_app_rols` (`idapp_rols`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `detalle_temp`
--
ALTER TABLE `detalle_temp`
  MODIFY `correlativo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `fmr_pharmacy`
--
ALTER TABLE `fmr_pharmacy`
  MODIFY `id_pharmacy` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `frm_box`
--
ALTER TABLE `frm_box`
  MODIFY `id_box` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `frm_client`
--
ALTER TABLE `frm_client`
  MODIFY `idcliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `frm_config`
--
ALTER TABLE `frm_config`
  MODIFY `id_config` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `frm_module`
--
ALTER TABLE `frm_module`
  MODIFY `id_module` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `frm_module_main_transaction`
--
ALTER TABLE `frm_module_main_transaction`
  MODIFY `id_module_main` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `frm_module_type`
--
ALTER TABLE `frm_module_type`
  MODIFY `idmodule_type` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `frm_product`
--
ALTER TABLE `frm_product`
  MODIFY `id_product` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `frm_provider`
--
ALTER TABLE `frm_provider`
  MODIFY `id_provider` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `frm_rols`
--
ALTER TABLE `frm_rols`
  MODIFY `id_rols` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `frm_rol_permission`
--
ALTER TABLE `frm_rol_permission`
  MODIFY `id_permission` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `frm_sales`
--
ALTER TABLE `frm_sales`
  MODIFY `id_sales` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1111101;
--
-- AUTO_INCREMENT de la tabla `frm_sales_detail`
--
ALTER TABLE `frm_sales_detail`
  MODIFY `id_sales_detail` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `frm_sales_unit`
--
ALTER TABLE `frm_sales_unit`
  MODIFY `id_sales_unit` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `frm_sub_module`
--
ALTER TABLE `frm_sub_module`
  MODIFY `id_submodule` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de la tabla `frm_type_pyment`
--
ALTER TABLE `frm_type_pyment`
  MODIFY `id_type_pyment` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `frm_type_sales`
--
ALTER TABLE `frm_type_sales`
  MODIFY `id_type_sales` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `frm_user`
--
ALTER TABLE `frm_user`
  MODIFY `id_user` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `frm_box`
--
ALTER TABLE `frm_box`
  ADD CONSTRAINT `fk_frm_box_frm_user1` FOREIGN KEY (`id_user`) REFERENCES `frm_user` (`id_user`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `frm_module_main_transaction`
--
ALTER TABLE `frm_module_main_transaction`
  ADD CONSTRAINT `fk_app_module_main_app_module1` FOREIGN KEY (`id_module`) REFERENCES `frm_module` (`id_module`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_app_module_main_app_module_type1` FOREIGN KEY (`id_module_type`) REFERENCES `frm_module_type` (`idmodule_type`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_app_module_main_app_rols1` FOREIGN KEY (`id_rols`) REFERENCES `frm_rols` (`id_rols`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `frm_product`
--
ALTER TABLE `frm_product`
  ADD CONSTRAINT `fk_frm_product_frm_provider1` FOREIGN KEY (`id_provider`) REFERENCES `frm_provider` (`id_provider`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_frm_product_frm_sales_unit1` FOREIGN KEY (`id_sales_unit`) REFERENCES `frm_sales_unit` (`id_sales_unit`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `frm_sales`
--
ALTER TABLE `frm_sales`
  ADD CONSTRAINT `fk_frm_sales_frm_type_pyment1` FOREIGN KEY (`id_type_pyment`) REFERENCES `frm_type_pyment` (`id_type_pyment`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_frm_sales_frm_type_sales1` FOREIGN KEY (`id_type_sales`) REFERENCES `frm_type_sales` (`id_type_sales`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `frm_sales_detail`
--
ALTER TABLE `frm_sales_detail`
  ADD CONSTRAINT `fk_frm_sales_detail_frm_sales` FOREIGN KEY (`id_sales`) REFERENCES `frm_sales` (`id_sales`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `frm_sub_module`
--
ALTER TABLE `frm_sub_module`
  ADD CONSTRAINT `fk_app_sub_module_app_module1` FOREIGN KEY (`id_module`) REFERENCES `frm_module` (`id_module`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `frm_user`
--
ALTER TABLE `frm_user`
  ADD CONSTRAINT `fk_app_user_app_rols` FOREIGN KEY (`idapp_rols`) REFERENCES `frm_rols` (`id_rols`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
