<?php

$alert = '';
$ema="";
session_start();
if (!empty($_SESSION['active'])) {
  header('location: sistema/index.php');
} else {
  if (!empty($_POST)) {
    if (empty($_POST['usuario']) || empty($_POST['clave'])) {
      $alert = '<div class="alert alert-danger" role="alert">
  Ingrese su usuario y su clave
</div>';
    } else {

      require_once "sistema/model/User.php";
      //https://www.emm-gfx.net/2008/11/encriptar-y-desencriptar-cadena-php/
      //https://www.w3schools.com/icons/fontawesome5_icons_code.asp
      //https://lacodigoteca.com/php/crear-ticket-en-pdf-con-php/#Si_dispones_de_algun_ERP_o_POS_y_lo_que_quieres_es_crear_ticket_en_PDF_o_una_factura_simplificada,_este_es_tu_POST
      $obju = new User();
      $userProfile = $obju->startUserSession($_POST['usuario'],base64_encode($_POST['clave']));
      $idUser = "";
      $nombre = "";
      $correo = "";
      $appell = "";
      $idrol = "";
      $rolname = "";
      if ($userProfile > 0) {
        foreach($userProfile as $prfl){
          $idUser =   $prfl[0];
          $nombre =   $prfl[1];
          $correo =   $prfl[2];
          $appell =   $prfl[3];
          $idrol =    $prfl[4];
          $rolname =  $prfl[5];
        }
        if($idUser!="" || $correo!=""){
          $_SESSION['active'] = true;
          $_SESSION['idUser'] = $idUser;
          $_SESSION['nombre'] = $nombre;
          $_SESSION['email'] = $correo;
          $_SESSION['surname'] = $appell;
          $_SESSION['rol'] = $idrol;
          $_SESSION['rol_name'] = $rolname;
          header('location: sistema');
          //header('location: sistema/');
        }else {
        $alert = '<div class="alert alert-danger" role="alert">
              Usuario o Contraseña Incorrecta
            </div>';
        session_destroy();
      }

      } 
    }
  }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="sistema/img/logo1.png" />

  <title>Eddie Farma</title>

  <!-- Custom fonts for this template-->
  <link href="sistema/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="sistema/css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body class="bg-gradient-primary" style="background: #43C6AC; 
background: -webkit-linear-gradient(to right, #F8FFAE, #43C6AC); 
background: linear-gradient(to right, #F8FFAE, #43C6AC);">

  <div class="container"> 
 
    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-6 d-none d-lg-block bg-login-image">
               <!--  <img src="sistema/img/logo.jpg" class="img-thumbnail"> -->
              </div>
              <div class="col-lg-6">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Iniciar Sesión</h1>
                    <?php echo $ema;?>
                  </div>
                  <form class="user" method="POST">
                    <?php echo isset($alert) ? $alert : ""; ?>
                    <div class="form-group">
                      <label for="">Email</label>
                      <input type="text" class="form-control form-control-user" placeholder="Email" name="usuario"></div>
                    <div class="form-group">
                      <label for="">Contraseña</label>
                      <input type="password" class="form-control form-control-user" placeholder="Contraseña" name="clave" >
                    </div>
                    <input type="submit" value="Iniciar " class="btn btn-primary btn-user btn-block" >
                    <hr>
                  </form>
                  <hr>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>
  
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="sistema/vendor/jquery/jquery.min.js"></script>
  <script src="sistema/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="sistema/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="sistema/js/sb-admin-2.min.js"></script>

</body>

</html>