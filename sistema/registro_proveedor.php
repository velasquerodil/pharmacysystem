<?php
include_once "includes/header.php";
require_once "controller/providerController.php";
if (!empty($_POST)) {
    $alert = "";
    if (empty($_POST['proveedor']) || empty($_POST['contacto']) || empty($_POST['telefono']) || empty($_POST['direccion'])) {
        $alert = '<div class="alert alert-danger" role="alert">
                        Todo los campos son obligatorios
                    </div>';
    } else {
        $proveedor = $_POST['proveedor'];
        $contacto = $_POST['contacto'];
        $telefono = $_POST['telefono'];
        $Direccion = $_POST['direccion'];
        $usuario_id = $_SESSION['idUser'];
        $contact = $_POST['person'];
        

        $provider = new ProviderController();
        $verifyPro = $provider->verifyRucController($contacto);

        if (!empty($verifyPro) ) {
            $alert = '<div class="alert alert-danger" role="alert">
                        Ruc ya existe, intente otro
                    </div>';
        }else{
        $saveP = $provider->saveProviderController("saveProvider",$proveedor,$Direccion,$contacto,$telefono,$contact,1);
        if ($saveP) {
            $alert = '<div class="alert alert-primary" role="alert" style="background: #4EDCBC;">
                        Proveedor Registrado
                    </div>';
        } else {
            $alert = '<div class="alert alert-danger" role="alert">
                       Error al registrar proveedor
                    </div>';
        }
        }
    }
}
?>

<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Content Row -->
    <div class="row">
        <div class="col-lg-6 m-auto">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <div class="d-sm-flex align-items-center justify-content-between">
                    <h5 class="m-0 font-weight-bold text-primary">Registro Proveedor</h5>
                        <a href="lista_proveedor.php" class="btn btn-primary">Regresar</a>
                    </div>
                </div>
                <div class="card-body">
                <form action="" autocomplete="off" method="post" class="card-body p-2">
                    <?php echo isset($alert) ? $alert : ''; ?>
                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input type="text" placeholder="Ingrese nombre" name="proveedor" id="nombre" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="contacto">Ruc</label>
                        <input type="text" placeholder="Ingrese nombre del contacto" name="contacto" id="contacto" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="telefono">Teléfono</label>
                        <input type="number" placeholder="Ingrese teléfono" name="telefono" id="telefono" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="direccion">Dirección</label>
                        <input type="text" placeholder="Ingrese Direccion" name="direccion" id="direcion" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="person">Contacto</label>
                        <input type="text" placeholder="Ingrese Persona de Contacto" name="person" id="person" class="form-control">
                    </div>
                    <input type="submit" value="Guardar Proveedor" class="btn btn-primary">
                   
                </form>
            </div>
        </div>
    
        </div>
    </div>


</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
<?php include_once "includes/footer.php"; ?>