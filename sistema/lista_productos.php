<?php include_once "includes/header.php"; ?>

<!-- Begin Page Content -->
<div class="container-fluid">

	<!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-white-800">Productos</h1>
		<a href="registro_producto.php" class="btn btn-primary">Nuevo</a>
	</div>

	<div class="card shadow mb-4">
		<div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Lista de Productos</h6>
         </div>
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-sm table-bordered" id="table" width="100%" cellspacing="0">
					<thead class="table" id="head">
						<tr style="font-size: 14px;"> 
							<th>Codigo Barra</th>
							<th>Nombre</th>
							<th>Marca</th>
							<th>Contenido</th>
							<th>Unidad venta</th>
							<th>Stock</th>
							<th>Precio Unitario</th>
							<th>Vencimiento</th>
							<th>Proveedor</th>
							<?php if ($_SESSION['rol'] == 1) { ?>
							<th>Acciones</th>
							<?php } ?>
						</tr>
					</thead>
					<tbody>
						<?php
						
						require_once "controller/ProductController.php";
						$objalu = new ProductController();
						$listar = $objalu->getProductsController("getProducts");
						if ($listar > 0) {
							foreach ($listar as $value) { ?>
								<tr style="background: white; font-size: 12px;">
									<td id="tr"><?php echo $value[1]; ?></td>
									<td id="tr"><?php echo $value[2]; ?></td>
									<td id="tr"><?php echo $value[3]; ?></td>
									<td id="tr"><?php echo $value[4]; ?></td>
									<td id="tr"><?php echo $value[5]; ?></td>
									<td id="tr"><?php echo $value[6]; ?></td>
									<td id="price"><?php echo "S/".$value[7]; ?></td>
									<td id="tr"><?php echo $value[8]; ?></td>
									<td id="tr"><?php echo $value[9]; ?></td>

										<?php if ($_SESSION['rol'] == 1) { ?>
									<td >
										<!--  <a href="agregar_producto.php?id=<?php //echo $value[0]; ?>" class="btn btn-primary"><i class='fas fa-audio-description'></i></a> -->
										<div class="row">
											<div style="display: inline-block;">
												<a href="editar_producto.php?id=<?php echo $value[0]; ?>" class="btn btn-success"><i class='fas fa-edit'></i></a>

											</div>
											<div style="display: inline-block; margin-left: 2px;">
												<form action="option/deleteProduct.php?id=<?php echo $value[0]; ?>" method="post" class="confirmar d-inline">
													<button class="btn btn-danger" type="submit"><i class='fas fa-trash-alt'></i> </button>
												</form>
											</div>
									</td>
										<?php } ?>
								</tr>
						<?php }
						} ?>
					</tbody>

				</table>
			</div>

		</div>
	</div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->


<?php include_once "includes/footer.php"; ?>