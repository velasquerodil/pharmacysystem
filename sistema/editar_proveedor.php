<?php

require_once "controller/providerController.php";
if (!empty($_POST)) {
  $alert = "";
  if (empty($_POST['proveedor']) || empty($_POST['contacto']) || empty($_POST['telefono']) || empty($_POST['direccion'])) {
    $alert = '<p class"msg_error">Todo los campos son requeridos</p>';
  } else {
    $idproveedor = $_GET['id'];
    $proveedor = $_POST['proveedor'];
    $ruc = $_POST['contacto'];
    $telefono = $_POST['telefono'];
    $direccion = $_POST['direccion'];
    $contact = $_POST['person'];

    $provider = new ProviderController();
    $sql_update = $provider->updateProviderController("editProvider",$idproveedor, $proveedor,$direccion, $ruc,$telefono,$contact);

    if ($sql_update) {
      $alert = '<p class"msg_save">Proveedor Actualizado correctamente</p>';
    } else {
      $alert = '<p class"msg_error">Error al Actualizar el Proveedor</p>';
    }
  }
}
// Mostrar Datos

if (empty($_REQUEST['id'])) {
  header("Location: lista_proveedor.php");
  mysqli_close($conexion);
}


$idproveedor = $_REQUEST['id'];

$provider = new ProviderController();
$verifyPro = $provider->findProviderByIDController($idproveedor);


$name = "";
$addres = "";
$ruc = "";
$phone = "";
$contact = "";


if (is_iterable($verifyPro) == 0) {
  header("Location: lista_proveedor.php");
} else {
  foreach ($verifyPro as $prov) {
    $name = $prov[1];
    $addres = $prov[2];
    $ruc = $prov[3];
    $phone = $prov[4];
    $contact = $prov[5];
  }
}
include "includes/header.php";
?>
<!-- Begin Page Content -->
<div class="container-fluid">

  <div class="row">
    <div class="col-lg-6 m-auto">

      <div class="card shadow mb-4">
          <div class="card-header py-3">
                    <div class="d-sm-flex align-items-center justify-content-between">
                    <h5 class="m-0 font-weight-bold text-primary">Editar Usuario</h5>
                        <a href="lista_proveedor.php" class="btn btn-primary">Regresar</a>
                    </div>
          </div>
          <div class="card-body">

            <form class="" action="" method="post">
              <?php echo isset($alert) ? $alert : ''; ?>
              <input type="hidden" name="id" value="<?php echo $idproveedor; ?>">
              <div class="form-group">
                <label for="proveedor">Proveedor</label>
                <input type="text" placeholder="Ingrese proveedor" name="proveedor" class="form-control" id="proveedor" value="<?php echo $name; ?>" required>
              </div>
              <div class="form-group">
                <label for="nombre">Ruc</label>
                <input type="text" placeholder="Ingrese contacto" name="contacto" class="form-control" id="contacto" value="<?php echo $ruc; ?>">
              </div>
              <div class="form-group">
                <label for="telefono">Teléfono</label>
                <input type="number" placeholder="Ingrese Teléfono" name="telefono" class="form-control" id="telefono" value="<?php echo $phone; ?>" required>
              </div>
              <div class="form-group">      
                <label for="direccion">Dirección</label>
                <input type="text" placeholder="Ingrese Direccion" name="direccion" class="form-control" id="direccion" value="<?php echo $addres; ?>">
            </div>
            <div class="form-group">
                  <label for="person">Contacto</label>
                  <input type="text" placeholder="Ingrese Persona de Contacto" name="person" id="person" class="form-control" value="<?php echo $contact; ?>">
            </div>

              <input type="submit" value="Editar Proveedor" class="btn btn-primary">
            </form>
          </div>
        </div>
    </div>
  </div>


</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
<?php include_once "includes/footer.php"; ?>