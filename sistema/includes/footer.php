<!-- Footer -->
<footer class="sticky-footer bg-white">
  <div class="container my-auto">
    <div class="copyright text-center my-auto">
      <span>Copyright &copy; Farmacia <?php echo date("Y"); ?></span>
    </div>
  </div>
</footer>
<!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
  <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        <a class="btn btn-primary" href="login.html">Logout</a>
      </div>
    </div>
  </div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="js/all.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="js/sb-admin-2.min.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap4.min.js"></script>
<script src="js/sweetalert2@10.js"></script>
<script src="js/bootstrap-datepicker.js"></script>


<script src="js/Buttons-1.6.5/js/dataTables.buttons.min.js"></script>
<script src="js/JSZip-2.5.0/jszip.min.js"></script>
<script src="js/pdfmake-0.1.36/pdfmake.min.js"></script>
<script src="js/pdfmake-0.1.36/vfs_fonts.js"></script>
<script src="js/Buttons-1.6.5/js/buttons.html5.min.js"></script>
<link rel="stylesheet" href="js/Buttons-1.6.5/css/buttons.dataTables.min.css">


<script type="text/javascript" src="js/producto.js"></script>
<script type="text/javascript">



    $(document).ready(function() {
        $('#table').DataTable( {
          responsive:"true",
            dom: 'Bfrtip',
            buttons: [
              {
                extend: 'excelHtml5',
                text: '<i class="fas fa-file-alt" style="color: #17A673;"></i>',
                titleAttr: 'exportar a excel',
                className:'btn btn-success'
              },
              {
                extend: 'pdfHtml5',
                text: '<i class="fas fa-file" style="color: #E02D1B;"></i>',
                titleAttr: 'exportar a pdf',
                className:'btn btn-danger'
              }
                
            ]
        } );
    } );

    $(document).ready(function() {
        $('#tablej').DataTable( {
            responsive:"true",
            dom: 'Bfrtip',
            paging: false,
            scrollX: true,
            scrollCollapse: true,
            ordering: false,
            bFilter: false, 
            bInfo: false,

            columnDefs:[{
              targets: "_all",
              sortable: false
            }],
            fixedColumns:   {
              leftColumns: 3
            },
            buttons: [
              {
                extend: 'excelHtml5',
                text: '<i class="fas fa-file-alt" style="color: #17A673;"></i>',
                titleAttr: 'exportar a excel',
                className:'btn btn-success'
              },
              {
                extend: 'pdfHtml5',
                text: '<i class="fas fa-file" style="color: #E02D1B;"></i>',
                titleAttr: 'exportar a pdf',
                className:'btn btn-danger'
              }
                
            ]
        } );
    } );

    

	$(document).ready( function(){
		$('.vertbtn').on('click', function(){
			$('#vermodal').modal('show');

        $str= $(this).closest('tr');
        var data = $str.childern("td").map(function(){
            return $(this).text();
        }).get();


        $('#password').val(data[2]);

		});
	});

/*
  setInterval(function() {
    $("html").load("http://localhost/pharmacysystem/sistema/registro_producto.php");

  },3000); */




  $(function() {
			$('.dates #fechaven').datepicker({
				'format': 'yyyy-mm-dd',
				'autoclose': true
			});
    });
    


</script>

</body>

</html>