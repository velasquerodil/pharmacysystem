<?php
include "includes/header.php";
require_once "controller/UserController.php";
require_once "controller/RoleController.php";
if (!empty($_POST)) {
  $alert = "";
  if (empty($_POST['nombre']) || empty($_POST['correo']) || empty($_POST['apellidos']) || empty($_POST['rol'])) {
    $alert = '<p class"error">Todo los campos son requeridos</p>';
  } else {
    $idusuario = $_GET['id'];
    $nombre = $_POST['nombre'];
    $correo = $_POST['correo'];
    $apellidos = $_POST['apellidos'];
    $rol = $_POST['rol'];
    $option="editUser";


    $objUser= new UserController();
    $objUser->editUserController($option,$idusuario,$rol,$nombre,$apellidos,$correo);
    $alert = '<div class="alert alert-primary" role="alert" style="background: #4EDCBC;">
                Usuario Actualizado
              </div>';
  }
}

// Mostrar Datos


$objUser= new UserController();

if (empty($_REQUEST['id'])) {
  header("Location: lista_usuarios.php");
}
$idusuario = $_REQUEST['id'];
$sql=$objUser->getuserByIdController($idusuario);

$idcliente ="";
$nombre = "";
$correo = "";
$app = "";
$rol = "";
if (is_iterable($sql) == 0) {
  header("Location: lista_usuarios.php");
} else {
  foreach ($sql as $user) {
      $idcliente = $user[0];
      $nombre = $user[1];
      $app = $user[2];
      $correo = $user[3];
      $rol = $user[4];
  }
}
?>


<!-- Begin Page Content -->
<div class="container-fluid">

  <div class="row">
    <div class="col-lg-6 m-auto">

      <div class="card shadow mb-4">
          <div class="card-header py-3">
                    <div class="d-sm-flex align-items-center justify-content-between">
                    <h5 class="m-0 font-weight-bold text-primary">Editar Usuario</h5>
                        <a href="lista_usuarios.php" class="btn btn-primary">Regresar</a>
                    </div>
            </div>
        <div class="card-body">
          <form class="" action="" method="post">
            <?php echo isset($alert) ? $alert : ''; ?>
            <input type="hidden" name="id" value="<?php echo $idusuario; ?>">

            <div class="form-group">
              <label for="nombre">Nombre</label>
              <input type="text" placeholder="Ingrese nombre" class="form-control" name="nombre" id="nombre" value="<?php echo $nombre; ?>" required>
            </div>

            <div class="form-group">
              <label for="apellidos">Apellidos</label>
              <input type="text" placeholder="Ingrese Apellidos" class="form-control" name="apellidos" id="apellidos" value="<?php echo $app; ?>" required>
            </div>

            
            <div class="form-group">
              <label for="correo">Correo</label>
              <input type="text" placeholder="Ingrese correo" class="form-control" name="correo" id="correo" value="<?php echo $correo; ?>" required>
            </div>
            <div class="form-group">
                  <label for="nombre">Rol</label>
                  <?php
                  $roleCon = new RoleController();
                  $listRolss = $roleCon->listRolesController();
                  ?>
                  <select id="rol" name="rol" class="form-control">
                    <option value="<?php echo $idcliente; ?>" selected><?php echo $rol; ?></option>
                    <?php
                    if ($listRolss > 0) {
                      foreach ($listRolss as $lsrols) {
                    ?>
                        <option value="<?php echo $lsrols[0]; ?>"><?php echo $lsrols[1]; ?></option>
                    <?php
                      }
                    }
                    ?>
                  </select>
            </div>
            <button type="submit" class="btn btn-primary"><i class="fas fa-user-edit"></i> Editar Usuario</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
<?php include_once "includes/footer.php"; ?>