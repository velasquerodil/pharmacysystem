<?php 
include_once "includes/header.php";

?>

<!--Begin Page Content-->
<div class="container-fluid">
        

    	<!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-white-800">Productos por vencer</h1>
	</div>

	<div class="card shadow mb-4">
		<div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Lista de Productos 3 semanas por vencer </h6>
         </div>
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-sm table-bordered" id="table" width="100%" cellspacing="0">
					<thead class="table" id="head">
						<tr style="font-size: 14px;"> 
							<th>Codigo Barra</th>
							<th>Nombre</th>
							<th>Marca</th>
							<th>Contenido</th>
							<th>Unidad venta</th>
							<th>Stock</th>
							<th>Precio Unitario</th>
							<th>Vencimiento</th>
							<th>Proveedor</th>
						</tr>
					</thead>
					<tbody>
						<?php
						
						require_once "controller/ProductController.php";
						$objalu = new ProductController();
						$listar = $objalu->getProductToExpireController();
						if ($listar > 0) {
							foreach ($listar as $value) { ?>
								<tr style="background: white; font-size: 12px;">
									<td id="tr"><?php echo $value[1]; ?></td>
									<td id="tr"><?php echo $value[2]; ?></td>
									<td id="tr"><?php echo $value[3]; ?></td>
									<td id="tr"><?php echo $value[4]; ?></td>
									<td id="tr"><?php echo $value[5]; ?></td>
									<td id="tr"><?php echo $value[6]; ?></td>
									<td id="price"><?php echo $value[7]; ?></td>
									<td style="color: red;"><?php echo $value[8]; ?></td>
									<td id="tr"><?php echo $value[9]; ?></td>

								</tr>
						<?php }
						} ?>
					</tbody>

				</table>
			</div>

		</div>
	</div>

</div>


</div>
<!-- End of Main Content -->
<?php include_once "includes/footer.php"; ?>