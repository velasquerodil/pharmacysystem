<?php 

include_once "includes/header.php"; 
require_once "controller/BoxController.php";


$saldo_inicial='';
$efectivo='';
$tarjeta='';
$credit='';
$otros='';
$total='';
$fecha='';
$saldo_final='';

if (!empty($_POST)) {
  $alert = "";
  if (empty($_POST['fechaven'])) {
        $alert = '<div class="alert alert-danger" role="alert">
                        Ingrese Fecha
                </div>';
  } else {
    $date = $_POST['fechaven'];

    $Box= new BoxController();
    $lst= $Box->getBoxDataController($date);
    if($lst>0){
            foreach($lst as $b){
                    
                $saldo_inicial=$b[0];
                $efectivo=$b[1];
                $tarjeta=$b[2];
                $credit=$b[3];
                $otros=$b[4];
                $total=$b[5];
                $fecha=$b[6];
                $saldo_final=$b[7];

            }

    }

  

  }
}




?>

<!-- Begin Page Content -->
<div class="container-fluid">

	

	<div class="card shadow mb-4">
		<div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Caja</h6>
                            <?php echo isset($alert) ? $alert : ''; ?>
         </div>
   
                 <form class="form-row"  style="margin-left: 15px; margin-top: 10px;" action="" method="post" autocomplete="off">
                 
                                            <div class="form-group col-md-2 dates">
                                                <label for="paga">Seleccione Fecha</label>
                                                    <input type="text"  placeholder="yyyy-mm-dd" class="form-control" id="fechaven" name="fechaven"  value="">
                                                </div>
                                                <div class="form-group col-md-4">
                                                        <input type="submit" value="Consultar"  style="margin-top: 32px;" class="btn btn-primary" >
                                                </div>
                </form>                            
   



		<div class="card-body">
			<div class="table-responsive">
                                <?php if($fecha!=''){ ?>
				<table class="tablej table-sm " id="tablej" width="100%" cellspacing="0">
					<thead class="tablej" id="head">
						<tr style="font-size: 14px; color: #F0F0F0; background: #F0F0F0;">
							<th>-</th>
							<th>-</th>
							<th>-</th>
							<th>-</th>
							<th>-</th>
							
						</tr>
					</thead>
					<tbody>
                                        
                                        
                                                <tr style="background: white; font-size: 13px;">
                                                                <td id="tr">Fecha : <?php echo $fecha;?></td>
                                                                <td id="tr"></td>
                                                                <td id="tr"></td>
                                                                <td id="tr">Preparado Por:</td>
                                                                <td id="tr"><?php echo $_SESSION['nombre'];?></td>			
                                                </tr>
                                                <tr style="background: white; font-size: 13px;">
						        <td id="tr"></td>
							<td id="tr"></td>
							<td id="tr"></td>
                                                        <td id="tr"></td>
                                                        <td id="tr"></td>			
                                                </tr>
					        <tr style="background: white; font-size: 13px;">
						        <td id="tr">Efectivo Inicial</td>
							<td id="tr"></td>
							<td id="tr"></td>
                                                        <td id="tr"></td>
                                                        <td id="tr" style="color: black; font-weight: bold;">S/<?php echo $saldo_inicial;?></td>			
                                                </tr>
                                                
                                                <tr style="background: white; font-size: 13px;">
						        <td id="tr">Ventas Diarias Totales</td>
							<td id="tr"></td>
							<td id="tr"></td>
                                                        <td id="tr"></td>
                                                        <td id="tr"></td>			
                                                </tr>
                                                
                                                <tr style="background: white; font-size: 13px;">
						        <td id="tr"></td>
							<td id="tr">Efectivo</td>
							<td id="tr"></td>
                                                        <td id="tr">S/<?php echo $efectivo;?></td>
                                                        <td id="tr"></td>			
                                                </tr>
                                                <tr style="background: white; font-size: 13px;">
						        <td id="tr"></td>
							<td id="tr">Tarjeta</td>
							<td id="tr"></td>
                                                        <td id="tr">S/<?php echo $tarjeta;?></td>
                                                        <td id="tr"></td>			
                                                </tr>
                                                <tr style="background: white; font-size: 13px; ">
						        <td id="tr"></td>
							<td id="tr">Crédito</td>
							<td id="tr"></td>
                                                        <td id="tr" >S/<?php echo $credit;?></td>
                                                        <td id="tr"></td>			
                                                </tr>

                                                <tr style="background: white; font-size: 13px; ">
						        <td id="tr"></td>
							<td id="tr">Otros</td>
							<td id="tr"></td>
                                                        <td id="tr" >S/<?php echo $otros;?></td>
                                                        <td id="tr"></td>			
                                                </tr>

                                                <tr style="background: white; font-size: 13px;">
						        <td id="tr"></td>
							<td id="tr"></td>
							<td id="tr">--></td>
                                                        <td id="tr" style="color: black">S/<?php echo $total;?></td>
                                                        <td id="tr"></td>			
                                                </tr>
                                                
                                                <tr style="background: white; font-size: 13px;">
						        <td id="tr"></td>
							<td id="tr"></td>
							<td id="tr"></td>
                                                        <td id="tr"></td>
                                                        <td id="tr"></td>			
                                                </tr>

                                                <tr style="background: #F0F0F0; font-size: 13px; ">
						        <td id="tr">Total En Efectico</td>
							<td id="tr"></td>
							<td id="tr"></td>
                                                        <td id="tr"></td>
                                                        <td id="tr" style="color: black; font-weight: bold;">S/<?php echo $saldo_final;?></td>			
                                                </tr>


						
					</tbody>

                                </table>
                                <?php }?>
			</div>

		</div>
	</div>


</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->


<?php include_once "includes/footer.php"; ?>