﻿<?php include_once "includes/header.php"; 

	require_once "controller/UserController.php";
?>

<?php
	$objUser= new UserController();
	$idcliente ="";
	$nombre = "";
	$correo = "";
	$app = "";
	$rol = "";

	if (!empty($_REQUEST['id'])) {

		$idusuario = $_REQUEST['id'];
		$sql=$objUser->getuserByIdController($idusuario);
		if (is_iterable($sql) == 0) {
		header("Location: lista_usuarios.php");
		} else {
		foreach ($sql as $user) {
			$idcliente = $user[0];
			$nombre = $user[1];
			$app = $user[2];
			$correo = $user[3];
			$rol = $user[4];
		}
	}
		
	}

?>

<!-- Begin Page Content -->
<div class="container-fluid">




	<!-- Button trigger modal
	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#staticBackdrop">
	Check
	</button>  -->

	<!-- Modal -->
	<div class="modal fade" id="vermodal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="staticBackdropLabel">Contraseña</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<form action="insertcode.php" method="POST">
		<div class="modal-body">

					<div class="form-group">
						<label for="password">Contraseña</label>
						<input type="text" class="form-control" placeholder="Ingrese Contraseña" value="<?php echo $correo  ?>" name="password" id="password">
					</div>
	
		</div>
		
		<div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
	
		</div>
		
		</form>
		</div>
	</div>
	</div>

	<!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800">Usuarios</h1>
		<?php if ($_SESSION['rol'] == 1) { ?>
		<a href="registro_usuario.php" class="btn btn-primary">Nuevo</a>
		<?php } ?>
	</div>

	<div class="card shadow mb-4">
		<div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Lista de Usuarios</h6>
         </div>
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-sm table-bordered" id="table" width="100%" cellspacing="0">
					<thead class="table" id="head">
						<tr style="font-size: 14px;">
							<th>Id</th>
							<th>Nombre</th>
							<th>Apellidos</th>
							<th>Correo</th>
							<th>Fecha creacion</th>
							<th>Fecha actualización</th>
							<th>Rol</th>
							<?php if ($_SESSION['rol'] == 1) { ?>
							<th>Contraseña</th>
							<th>Acciones</th>
							<?php }?>
						</tr>
					</thead>
					<tbody>
						<?php
						require_once "controller/UserController.php";
						$objalu = new UserController();
						$listar = $objalu->listUsersController();	

						if ($listar > 0) {
							foreach ($listar as $user) { ?>
								<tr style="background: white; font-size: 13px;">
									<td id="tr"><?php echo $user[0]; ?></td>
									<td id="tr"><?php echo $user[1]; ?></td>
									<td id="tr"><?php echo $user[2]; ?></td>
									<td id="tr"><?php echo $user[3]; ?></td>
									<td id="tr"><?php echo $user[4]; ?></td>
									<td id="tr"><?php echo $user[5]; ?></td>
									<td id="tr"><?php echo $user[6]; ?></td>
									<?php if ($_SESSION['rol'] == 1) { ?>
									<td id="tr"><!--<a href="lista_usuarios.php?id=" class="btn btn-success" data-toggle="modal" data-target="#staticBackdrop" > <i class='fas fa-eye' ></i></a> -->
									<!-- <button type="button" class="btn btn-success vertbtn" > Ver</button> -->
									<?php echo base64_decode($user[7]); ?>
									</td>

									<?php } ?>
									<?php if ($_SESSION['rol'] == 1) { ?>
									<td id="tr">

									<div class="row" >
										<div style="display: inline-block;">
											<a href="editar_usuario.php?id=<?php echo $user[0]; ?>" class="btn btn-success btn-xs"><i class='fas fa-edit'></i></a>
										</div>
										<div style="display: inline-block; margin-left: 2px;">
											<form action="option/deleteUser.php?id=<?php echo $user[0]; ?>" method="post" class="confirmar d-inline">
												<button class="btn btn-danger btn-xs" type="submit"><i class='fas fa-trash-alt'></i> </button>
											</form>
										</div>
									</div>	
									</td>
									<?php } ?>
								</tr>
						<?php }
						} ?>
					</tbody>

				</table>
			</div>

		</div>
	</div>


</div>
<!-- /.container-fluid -->




</div>
<!-- End of Main Content -->


<?php include_once "includes/footer.php"; ?>