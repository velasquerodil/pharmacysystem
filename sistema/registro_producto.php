 <?php include_once "includes/header.php";
  require_once "controller/ProductController.php";
  require_once "controller/providerController.php";
  include_once "firebaseConfig.php";

  if (!empty($_POST)) {
    $alert = "";
    if (empty($_POST['proveedor']) || empty($_POST['producto']) || empty($_POST['precio']) || $_POST['precio'] <  0 || empty($_POST['cantidad'] || $_POST['cantidad'] <  0)) {
      $alert = '<div class="alert alert-danger" role="alert">
                Todo los campos son obligatorios
              </div>';
    } else {
      $proveedor = $_POST['proveedor'];
      $producto = $_POST['producto'];
      $cantidad = $_POST['cantidad'];
      $usuario_id = $_SESSION['idUser'];
      $barcode = $_POST['cbarra'];
      $brand = $_POST['brand'];
      $presentation = $_POST['present'];
      $purchaseprice = $_POST['precio'];
      $SalesUnit = $_POST['salesunit'];
      $Salesprice = $_POST['saleprice'];
      $expiration = $_POST['fechaven'];
      $stock = $_POST['cantidad'];

      $fbdata = [
        'proveedor' => $proveedor,
        'producto' => $producto,
        'Cantidad' => $cantidad,
        'codigo' => $barcode
      ];

      $objalu = new ProductController();
      $isbarcod = $objalu->verifyBarCodeController($barcode);
      if (!empty($isbarcod) > 0) {
        $alert = '<div class="alert alert-danger" role="alert">
                    Codigo de barra ya existe
                </div>';
      } else {

        $query_insert = $objalu->saveProductController("saveProduct", $proveedor, $usuario_id, $barcode, $producto, $brand, $presentation, $purchaseprice, $SalesUnit, $Salesprice, $expiration, $stock, 1);
        if ($query_insert) {
          $alert = '<div class="alert alert-primary" role="alert" style="background: #4EDCBC;">
              Producto Registrado
            </div>';
          $ref = "product";
          $postdata = $database->getReference($ref)->remove();
        } else {
          $alert = '<div class="alert alert-danger" role="alert">
                  Error al registrar el producto
                </div>';
        }
      }
    }
  }
  /*
  $reff = "product";
  $reference  = $database->getReference($reff)->getValue();
  $productnsme="";

    foreach($reference as $key => $row){
      $productnsme = $row['producto'];
    }
*/
  ?>

 <script src="vendor/jquery/jquery.min.js"></script>
 <script type="text/javascript">
   //https://github.com/Judaskira/CameraBarcodeQRScanner

   $(document).ready(function() {
     var idInter;
     $('#textbox1').val(this.checked);
     $('#checkbox1').change(function() {
       if (this.checked) {
         //alert("recibiendo data");
         $('#textbox1').val(this.checked);
         var alerta = '<marquee style="color: #4EDCBC;">Recibiendo....</marquee>';
         $('#infop').html(alerta);

         try {
           idInter = setInterval(function() {
             var action = 'getp';
             $.ajax({
               method: "POST",
               url: "firebaseProduct.php", // Podrías separar las funciones de PHP en un fichero a parte
               data: {
                 action: action
               },
               success: function(response) {
                 if (response != 0) {
                   var info = JSON.parse(response);
                   var cod = "";
                   /*
                   for(var dat in info){
                     cod=info[dat].codigo;
                   }*/
                   //console.log(cod);
                   console.log(info.codigo);
                   $("#cbarra").val(info.codigo);
                   $("#cbarra").style.color='#FF5733'
                   //document.getElementById('myParagraph').style.color = '#000000';
                 }
               },
               error: function(error) {}
             });
           }, 1000); // run eeach 1 seg;

         } catch (error) {
           console.error(error);
         }
       } else {
         console.log("parando...");
         clearInterval(idInter);
         var alerta = 'Recibir data';
         $('#infop').html(alerta);
       }
     });




   });
 </script>
 <!-- Begin Page Content -->
 <div class="container-fluid">

   <!-- Page Heading -->
   <div class="d-sm-flex align-items-center justify-content-between mb-4">
     <h1 class="h3 mb-0 text-gray-800">Nuevo Producto </h1>
     <!-- <a href="lista_productos.php" class="btn btn-primary">Regresar</a>-->
   </div>


   <div class="card shadow mb-4">
     <div class="card-header py-3">
       <div class="d-sm-flex align-items-center justify-content-between">
         <h5 class="m-0 font-weight-bold text-primary">Registro Producto </h5>
         <a href="lista_productos.php" class="btn btn-primary">Regresar</a>


       </div>

       <div style="margin-left: 20px; width: 100px;">
         <input type="checkbox" class="form-check-input" id="checkbox1">
         <p id="infop">Recibir data </p>


       </div>

     </div>
     <div class="card-body">

       <!-- Content Row -->
       <div class="row">
         <div class="col-lg-8 m-auto">



           <form action="" method="post" autocomplete="off">
             <?php echo isset($alert) ? $alert : ''; ?>

             <div class="form-group">
               <label>Proveedor</label>
               <?php

                $provider = new ProviderController();
                $listprov = $provider->listProviderController();
                ?>
               <select id="proveedor" name="proveedor" class="form-control">
                 <?php
                  if ($listprov > 0) {
                    foreach ($listprov as $proveedor) {
                      // code...
                  ?>
                     <option value="<?php echo $proveedor[0]; ?>"><?php echo $proveedor[1]; ?></option>
                 <?php
                    }
                  }
                  ?>
               </select>
             </div>
             <div class="form-row">

               <div class="form-group col-md-4">
                 <label for="cbarra">Código</label>
                 <input type="text" placeholder="Ingrese Codigo de Barra" class="form-control" name="cbarra" id="cbarra" style="background: #E8F8F5; color: #3498DB; ">
               </div>
               <div class="form-group col-md-6">
                 <label for="producto">Producto</label>
                 <input type="text" placeholder="Ingrese nombre del producto" name="producto" id="producto" class="form-control" required>
               </div>
               <div class="form-group col-md-2">
                 <label for="precio">Precio Compra</label>
                 <input type="number" step="any" placeholder="00.00" class="form-control" name="precio" id="precio" required>
               </div>
             </div>

             <div class="form-row">

               <div class="form-group col-md-4">
                 <label for="brand">Marca</label>
                 <input type="text" placeholder="Ingrese Marca" class="form-control" name="brand" id="brand">
               </div>
               <div class="form-group col-md-6">
                 <label for="salesunit">Unidad de venta</label>

                 <?php

                  $objSalesUnit = new ProductController();
                  $ltSalesUnit = $objSalesUnit->getSalesUnitController();
                  ?>

                 <select id="salesunit" name="salesunit" class="form-control">

                   <?php
                    if ($ltSalesUnit > 0) {
                      foreach ($ltSalesUnit  as $slunit) {
                        // code...
                    ?>
                       <option value="<?php echo $slunit[0]; ?>"><?php echo $slunit[1]; ?></option>

                   <?php
                      }
                    }
                    ?>
                 </select>
               </div>


               <div class="form-group col-md-2">
                 <label for="saleprice">Precio Unitario</label>
                 <input type="number" step="any" placeholder="00.00" class="form-control" name="saleprice" id="saleprice" required>
               </div>
             </div>
             <div class="form-row">

               <div class="form-group col-md-4">
                 <label for="present">Contenido</label>
                 <input type="text" placeholder="200 ML, 200GR, 1KG, 1LT" class="form-control" name="present" id="present">
               </div>

               <div class="form-group col-md-2">
                 <label for="cantidad">Cantidad</label>
                 <input type="number" placeholder="00" class="form-control" name="cantidad" id="cantidad" required>
               </div>

               <div class="dates">
                 <label>Fecha de vencimiento</label>
                 <input type="text" style="width:130px;background-color:#aed6f1;" class="form-control" id="fechaven" name="fechaven" placeholder="yyyy-mm-dd" autocomplete="off" required>
               </div>


             </div>

             <input type="submit" value="Guardar Producto" class="btn btn-primary">




           </form>
         </div>
       </div>
     </div>
   </div>

 </div>
 <!-- /.container-fluid -->
 </div>

 <!-- End of Main Content -->
 <?php include_once "includes/footer.php";


  ?>