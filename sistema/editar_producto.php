<?php
include "includes/header.php"; 

require_once "controller/ProductController.php";
require_once "controller/providerController.php";
if (!empty($_POST)) {
  $alert = "";
  if (empty($_POST['producto']) || empty($_POST['precio'])) {
    $alert = '<div class="alert alert-primary" role="alert">
              Todo los campos son requeridos
            </div>';
  } else {
      $codproducto1 = $_GET['id'];
      $proveedor1 = $_POST['proveedor'];
      $producto1 = $_POST['producto'];
      $cantidad1 = $_POST['cantidad'];
      $usuario_id1 = $_SESSION['idUser'];
      $barcode1= $_POST['cbarra'];
      $brand1= $_POST['brand'];
      $presentation1= $_POST['present'];
      $purchaseprice1= $_POST['precio'];
      $SalesUnit1= $_POST['salesunit'];
      $Salesprice1= $_POST['saleprice'];
      $expiration1= $_POST['fechaven'];
      $stock1= $_POST['cantidad'];


      $objProd= new ProductController();


    $query_update = $objProd->updateProductController("editProduct",$codproducto1,$proveedor1,$usuario_id1,$barcode1,$producto1,$brand1,
                                                      $presentation1,$purchaseprice1,$SalesUnit1,$Salesprice1,$expiration1,$stock1);
    if ($query_update) {
      $alert = '<div class="alert alert-primary" role="alert" style="background: #4EDCBC;">
              Actualizado
            </div>';
    } else {
      $alert = '<div class="alert alert-primary" role="alert">
                Error al Modificar
              </div>';
    }
  }
}

// Validar producto

if (empty($_REQUEST['id'])) {
  header("Location: lista_productos.php");
} else {

    $id_producto = $_REQUEST['id'];
    $idprovider="";
    $iduser="";
    $barcode="";
    $name="";
    $brand="";
    $presentation="";
    $purchaseprice="";
    $salesunit="";
    $salesprice="";
    $expiration="";
    $stock="";
  if (!is_numeric($id_producto)) {
    header("Location: lista_productos.php");
  }

  $objP= new ProductController();
  $ltProduct= $objP->findProductbyIdController($id_producto);

  if ($ltProduct > 0) {
    foreach($ltProduct as $prod){
      $idprovider =   $prod[0];
      $barcode=       $prod[1];
      $name=          $prod[2];
      $brand=         $prod[3];
      $presentation=  $prod[4];
      $purchaseprice= $prod[5];
      $salesunit=     $prod[6];
      $stock=         $prod[7];
      $salesprice=    $prod[8];
      $expiration=    $prod[9];
    }
  } else {
    header("Location: lista_productos.php");
  }
}
?>
<!-- Begin Page Content -->

 <!-- Begin Page Content -->
 <div class="container-fluid">

   <!-- Page Heading -->
   <div class="d-sm-flex align-items-center justify-content-between mb-4">
     <h1 class="h3 mb-0 text-gray-800">Editar Producto</h1>
     <!-- <a href="lista_productos.php" class="btn btn-primary">Regresar</a>-->
   </div>


   <div class="card shadow mb-4">
            <div class="card-header py-3">
                  <div class="d-sm-flex align-items-center justify-content-between">
                    <h5 class="m-0 font-weight-bold text-primary">Actualiar Producto</h5>
                        <a href="lista_productos.php" class="btn btn-primary">Regresar</a>
                    </div>
              </div>
            <div class="card-body">
              
   <!-- Content Row -->
            <div class="row">
              <div class="col-lg-8 m-auto">


              <form action="" method="post" autocomplete="on">
                <?php echo isset($alert) ? $alert : ''; ?>
                <div class="form-group">
                  <label>Proveedor</label>
                  <?php

                    $provider = new ProviderController();
                    $listprov = $provider->listProviderController();
                    $prov = $provider->findProviderByIDController($idprovider);
                    $idprov=0;
                    $pname="";
                    foreach($prov as $p){
                      $idprov=$p[0];
                      $pname=$p[1];
                    }
                    ?>
                  <select id="proveedor" name="proveedor" class="form-control">
                      <option value="<?php echo $idprov;?>" selected><?php echo $pname; ?></option>
                        <?php
                      if ($listprov> 0) {
                        foreach($listprov as $proveedor) {
                      ?>
                        <option value="<?php echo $proveedor[0]; ?>"><?php echo $proveedor[1]; ?></option>
                    <?php
                        }
                      }
                      ?>
                  </select>
                </div>
                <div class="form-row">

                    <div class="form-group col-md-4">
                      <label for="cbarra">Código</label>
                      <input type="text" placeholder="Ingrese Codigo de Barra" class="form-control" name="cbarra" id="cbarra" value="<?php echo $barcode;?>">
                    </div>
                    <div class="form-group col-md-6">
                      <label for="producto">Producto</label>
                      <input type="text" placeholder="Ingrese nombre del producto" name="producto" id="producto" class="form-control" value="<?php echo $name;?>" required>
                    </div>
                    <div class="form-group col-md-2">
                      <label for="precio">Precio Compra</label>
                      <input type="text" placeholder="00.00" class="form-control" name="precio" id="precio" value="<?php echo $purchaseprice;?>" required>
                    </div>
                  </div>  

                  <div class="form-row">

                    <div class="form-group col-md-4">
                      <label for="brand">Marca</label>
                      <input type="text" placeholder="Ingrese Marca" class="form-control" name="brand" id="brand" value="<?php echo $brand;?>">
                    </div>
                    <div class="form-group col-md-6">
                      <label for="salesunit">Unidad de venta</label>

                      <?php

                      $objSalesUnit= new ProductController();
                      $ltSalesUnit = $objSalesUnit->getSalesUnitController();
                      $ltSUnit = $objSalesUnit->findSalesUnitByIdController($salesunit);
                      $sid=0;
                      $sname="";
                      foreach($ltSUnit as $s){
                        $sid=$s[0];
                        $sname=$s[1];

                      }
                      ?>

                      <select id="salesunit" name="salesunit" class="form-control">
                      <option value="<?php echo $sid; ?>"><?php echo $sname; ?></option>
                      
                        <?php
                        if ($ltSalesUnit> 0) {
                          foreach($ltSalesUnit  as $slunit) {
                        ?>
                          <option value="<?php echo $slunit[0]; ?>"><?php echo $slunit[1]; ?></option>

                          <?php
                          }
                        }
                      ?>
                    </select>
                    </div>


                    <div class="form-group col-md-2">
                      <label for="saleprice">Precio Unitario</label>
                      <input type="text" placeholder="00.00" class="form-control" name="saleprice" id="saleprice" value="<?php echo $salesprice;?>" required>
                    </div>
                  </div>  
                  <div class="form-row">  

                  <div class="form-group col-md-4">
                      <label for="present">Contenido</label>
                      <input type="text" placeholder="200 ML, 200GR, 1KG, 1LT" class="form-control" name="present" id="present" value="<?php echo $presentation;?>">
                    </div>  
                    
                    <div class="form-group col-md-2">
                      <label for="cantidad">Cantidad</label>
                      <input type="number" placeholder="00" class="form-control" name="cantidad" id="cantidad" value="<?php echo $stock;?>" required>
                    </div>

                    <div class="dates" >
                      <label>Fecha de vencimiento</label>
                      <input type="text" style="width:130px;background-color:#aed6f1;"  class="form-control" id="fechaven" name="fechaven" placeholder="yyyy-mm-dd" autocomplete="off" value="<?php echo $expiration;?>" required>
                    </div>

                    
                  </div>
              
                <input type="submit" value="Actualizar Producto" class="btn btn-primary">
              </form>

          </div>
          </div>
     </div>
  </div>


 </div>
 <!-- /.container-fluid -->

 </div>
 <!-- End of Main Content -->
 <?php include_once "includes/footer.php"; ?>