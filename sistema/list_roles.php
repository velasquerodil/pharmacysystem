<?php include_once "includes/header.php"; ?>

<!-- Begin Page Content -->
<div class="container-fluid">
		<!-- Button trigger modal 
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#staticBackdrop">
  Check
</button>-->

<!-- Modal -->
<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Agreagr Usuarios</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
	  </div>
	  <form action="insertcode.php" method="POST">
      <div class="modal-body">

	  			<div class="form-group">
                    <label for="nombre">Nombre</label>
                    <input type="text" class="form-control" placeholder="Ingrese Nombre" name="nombre" id="nombre">
                </div>

                <div class="form-group">
                    <label for="descripcion">Decripcion</label>
                    <input type="text" class="form-control" placeholder="Ingrese Descripcion" name="descripcion" id="descripcion">
                </div>
 
	  </div>
	  
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" name="insetdata" class="btn btn-primary">Understood</button>
	  </div>
	  
	  </form>
    </div>
  </div>
</div>


	<!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800">Roles</h1>
		<?php if ($_SESSION['rol'] == 1) { ?>
		<a href="add_permissions.php" class="btn btn-primary">Nuevo</a>
		<?php } ?>
	</div>

	<div class="card shadow mb-4">
		<div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Roles</h6>
         </div>
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-sm table-bordered" id="table" width="100%" cellspacing="0">
					<thead class="table" id="head">
						<tr style="font-size: 14px;">
							<th>IdRol</th>
							<th>Nombre</th>
							<th>Descripcion</th>
							<th>Estado</th>
							<?php if ($_SESSION['rol'] == 1) { ?>
							<th>Accion</th>
							<?php } ?>
						</tr>
					</thead>
					<tbody>
						<?php
						require_once "controller/RoleController.php";
						$objalu = new RoleController();
						$listar = $objalu->listRolesController();	

						if ($listar > 0) {
							foreach ($listar as $rl) { ?>
								<tr style="background: white; font-size: 13px;">
									<td id="tr"><?php echo $rl[0]; ?></td>
									<td id="tr"><?php echo $rl[1]; ?></td>
									<td id="tr"><?php echo $rl[2]; ?></td>
									<td id="tr"><a href="#" class="btn btn-success btn-circle"><i class="fas fa-check" ></i></a></td>
									<?php if ($_SESSION['rol'] == 1) { ?>
                                    <td id="tr">
                                        <a href="edit_roles.php?id=<?php echo $rl[0];?>" class="btn btn-success"><i class='fas fa-edit'></i> Editar</a>
									</td>
									<?php } ?>
									
								</tr>
						<?php }
						} ?>
					</tbody>

				</table>
			</div>

		</div>
	</div>


</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->


<?php include_once "includes/footer.php"; ?>