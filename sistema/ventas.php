<?php include_once "includes/header.php"; ?>

<!-- Begin Page Content -->
<div class="container-fluid">

	<!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800">Ventas</h1>
	</div>
	<div class="card shadow mb-4">
		<div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Registro de ventas</h6>
         </div>
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-striped table-bordered" id="table">
					<thead class="table" id="head">
						<tr style="font-size: 14px;">
							<th>Id</th>
							<th>Emisión</th>
							<th>Tipo Pago</th>
							<th>Fecha</th>
							<th>Hora</th>
							<th>Total</th>
							<th>Acciones</th>
						</tr>
					</thead>
					<tbody>
						<?php
						require_once "controller/SalesController.php";
						$objS = new SalesController();
        				$listProd = $objS ->listSalesController();
				
						if ($listProd> 0) {
							foreach ($listProd as $dato) {
						?>
								<tr style="background: white; font-size: 12px;">
									<td id="tr"><?php echo $dato[0]; ?></td>
									<td id="tr"><?php echo $dato[1]; ?></td>
									<td id="tr"><?php echo $dato[2]; ?></td>
									<td id="tr"><?php echo $dato[3]; ?></td>
									<td id="tr"><?php echo $dato[4]; ?></td>
									<td id="price"><?php echo $dato[5]; ?></td>
									<td>
										<button type="button" class="btn btn-primary view_factura" f="<?php echo $dato[0]; ?>">Ver</button>
									</td>
								</tr>
						<?php }
						} ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>



</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->


<?php include_once "includes/footer.php"; ?>