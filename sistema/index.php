<?php include_once "includes/header.php"; 
require_once "controller/DashboardController.php";




//metricas
$dash_sales='';
$dash_clients='';
$dash_providers='';
$dash_users='';
$dash_products='';



$objdash = new DashboardController();
$listardash = $objdash->listDashboardDataController();
foreach($listardash as $dash){
	$dash_users=$dash[0];
	$dash_clients=$dash[1];
	$dash_providers=$dash[2];
	$dash_products=$dash[3];
	$dash_sales=$dash[4];
	
	
	
	

}

?>

<!-- Begin Page Content -->
<div class="container-fluid">

	<!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800">Panel de Administración</h1>
	</div>

	<!-- Content Row -->
	<div class="row">



		<!-- Earnings (Monthly) Card Example -->
		<a class="col-xl-3 col-md-6 mb-4" href="lista_usuarios.php">
			<div class="card border-left-primary shadow h-100 py-2">
				<div class="card-body">
					<div class="row no-gutters align-items-center">
						<div class="col mr-2">
							<div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Usuarios</div>
							<div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $dash_users; ?></div>
						</div>
						<div class="col-auto">
							<i class="fas fa-user fa-2x text-gray-300"></i>
						</div>
					</div>
				</div>
			</div>
		</a>

		<!-- Earnings (Monthly) Card Example -->
		<a class="col-xl-3 col-md-6 mb-4" href="lista_cliente.php">
			<div class="card border-left-success shadow h-100 py-2">
				<div class="card-body">
					<div class="row no-gutters align-items-center">
						<div class="col mr-2">
							<div class="text-xs font-weight-bold text-success text-uppercase mb-1">Clientes</div>
							<div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $dash_clients; ?></div>
						</div>
						<div class="col-auto">
							<i class="fas fa-users fa-2x text-gray-300"></i>
						</div>
					</div>
				</div>
			</div>
		</a>

		<!-- Earnings (Monthly) Card Example -->
		<a class="col-xl-3 col-md-6 mb-4" href="lista_productos.php">
			<div class="card border-left-info shadow h-100 py-2">
				<div class="card-body">
					<div class="row no-gutters align-items-center">
						<div class="col mr-2">
							<div class="text-xs font-weight-bold text-info text-uppercase mb-1">Productos</div>
							<div class="row no-gutters align-items-center">
								<div class="col-auto">
									<div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"><?php echo $dash_products; ?></div>
								</div>
								<div class="col">
									<div class="progress progress-sm mr-2">
										<div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-auto">
							<i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
						</div>
					</div>
				</div>
			</div>
		</a>

		<!-- Pending Requests Card Example -->
		<a class="col-xl-3 col-md-6 mb-4" href="ventas.php">
			<div class="card border-left-warning shadow h-100 py-2">
				<div class="card-body">
					<div class="row no-gutters align-items-center">
						<div class="col mr-2">
							<div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Ventas</div>
							<div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $dash_sales; ?></div>
						</div>
						<div class="col-auto">
							<i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
						</div>
					</div>
				</div>
			</div>
		</a>
	</div>
	<!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800">Detalles</h1>
	</div>


	<div class="row" >
        <div class="col-sm-6" >

			<div class="card shadow ">
				<div class="card-header py-3">
									<h6 class="m-0 font-weight-bold text-primary">Productos mas Vendidos</h6>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-sm table-bordered" id="tablex" width="100%" cellspacing="0">
							<thead class="tablex" id="head">
								<tr style="font-size: 14px;">
									<th>Veces vendidos</th>
									<th>Nombre</th>
									<th>Total</th>

								</tr>
							</thead>
							<tbody>
								<?php
								
								
								$listar = $objdash->getProductBestSellerController();

								if ($listar > 0) {
									foreach ($listar as $user) { ?>
										<tr style="background: white; font-size: 13px;">
											<td id="tr"><?php echo $user[1]; ?></td>
											<td id="tr"><?php echo $user[2]; ?></td>
											<td id="price"><?php echo $user[3]; ?></td>
										
											
										</tr>
								<?php }
								} ?>
							</tbody>

						</table>
					</div>

				</div>
			</div>                       
		</div>
		
		<div class="col-sm-6" >

			<div class="card shadow ">
				<div class="card-header py-3">
									<h6 class="m-0 font-weight-bold text-primary">Ultimas ventas</h6>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-sm table-bordered" id="tablex" width="100%" cellspacing="0">
							<thead class="tablex" id="head">
								<tr style="font-size: 14px;">
									<th>Emisión</th>
									<th>Tipo Pago</th>
									<th>Vendido Por</th>
									<th>Fecha</th>
									<th>Total</th>

								</tr>
							</thead>
							<tbody>
								<?php
								
								
								$listar = $objdash->getLastSalesController();

								if ($listar > 0) {
									foreach ($listar as $user) { ?>
										<tr style="background: white; font-size: 13px;">
											<td id="tr"><?php echo $user[1]; ?></td>
											<td id="tr"><?php echo $user[2]; ?></td>
											<td id="tr"><?php echo $user[3]; ?></td>
											<td id="tr"><?php echo $user[4]." / ".$user[5]; ?></td>
											<td id="price"><?php echo $user[6]; ?></td>
										
											
										</tr>
								<?php }
								} ?>
							</tbody>

						</table>
					</div>

				</div>
			</div>                       
		</div>

		



	</div>	
	
								
	

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->


<?php include_once "includes/footer.php"; ?>