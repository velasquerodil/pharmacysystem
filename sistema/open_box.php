<?php 
include_once "includes/header.php";
require_once "controller/SalesController.php";
require_once "controller/BoxController.php";



if (!empty($_POST)) {
        $alert = "";
        if (empty($_POST['descripbox']) || empty($_POST['montobox'])) {
            $alert = '<div class="alert alert-danger" role="alert">
                                        Todo los campos son obligatorio
                                    </div>';
        } else {
                
                $usuario_id = $_SESSION['idUser'];
                $mont = $_POST['montobox'];
                $direccion = $_POST['descripbox'];
            
    
            
            $isActive='';
            $ObjBox= new SalesController();
            $Box= new BoxController();
            $listB = $ObjBox->verifyBoxController();
            foreach($listB as $b){
                $isActive=$b[0];

            }
    
        
            if ($isActive=="1") {
                $alert = '<div class="alert alert-danger" role="alert">
                                        ¡Caja ya se encuentra Aperturado! 
                                    </div>';
            } else {
                $query_insert = $Box->openBoxController($usuario_id, $mont);
                if ($query_insert) {
                    $alert = '<div class="alert alert-primary" role="alert" style="background: #4EDCBC;">
                                        ¡Caja Aperturado!, Ya puede inicar con su venta
                                    </div>';
                } else {
                    $alert = '<div class="alert alert-danger" role="alert">
                                        Error al Abrir Caja
                                </div>';
                }
            }
        }
    }

?>

<!--Begin Page Content-->
<div class="container-fluid">


        <h2>Apertura Caja</h2>

        <div class="col-lg-10 m-auto">
            <div class="card shadow mb-4">
                <div class="card-header py-3" >
                    <div class="d-sm-flex align-items-center justify-content-between">
                    <h5 class="m-0 font-weight-bold text-primary">Caja</h5>
              
                    </div>
                </div>


                <div class="row">
              
                        <div class="col-sm-8" >

                                <div class="card-body">
                                        <form action="" method="post" autocomplete="off">
                                                <div id="idalertbox">
                                                <?php echo isset($alert) ? $alert : ''; ?>
                                                </div>
                                        
                                                

                                                <div class="form-group row">
                                                <label for="inputEmail3" class="col-sm-2 col-form-label">Descrip.</label>
                                                <div class="col-sm-6">
                                                <input type="text" class="form-control" id="descripbox" name="descripbox" placeholder="Descripcion">
                                                </div>
                                                </div>
                                                <div class="form-group row">
                                                        <label for="inputPassword3" class="col-sm-2 col-form-label">Monto</label>
                                                        <div class="col-sm-6">
                                                        <input type="number" step="any"  class="form-control" id="montobox" name="montobox" placeholder="00.00">
                                                        </div>
                                                </div>
                                                <div class="form-group row">
                                                        <label for="inputPassword3" class="col-sm-2 col-form-label">Fecha</label>
                                                        <div class="col-sm-4">
                                                        <input type="text" class="form-control" id="inputPassword3" placeholder="Password" value="<?php echo date("Y-m-d");?>" disabled>
                                                        </div>
                                                </div>

                                                <div class="form-group row">
                                                        <label for="inputPassword3" class="col-sm-2 col-form-label"> Hora </label>
                                                        <div class="col-sm-4">
                                                        <input type="text" class="form-control" id="inputPassword3" placeholder="Password" value="<?php echo date("H:i:s");?>" disabled>
                                                        </div>
                                                </div>
                                        

                                                <input type="submit" value="Abrir Caja" class="btn btn-success">
                                        </form>
                                </div>
                        </div>
                        <div class="col-sm-4">
                                
                                <div class="form-row" style="margin-top: 15px;">
                                        <div class="form-group col-md-10">
                                
                                                <div class="card border-left-primary shadow h-100 py-2">
                                                                        <div class="card-body">
                                                                                <div class="row no-gutters align-items-center">
                                                                                <?php 
                                                                                $isActiveDD='';
                                                                                $ObjBox= new SalesController();
                                                                                $Box= new BoxController();
                                                                                $listB = $ObjBox->verifyBoxController();
                                                                                foreach($listB as $b){
                                                                                    $isActiveDD=$b[0];
                                                                    
                                                                                }
                                                                                
                                                                                
                                                                                if($isActiveDD=="1"){?>

                                                                                <form action="" method="post" autocomplete="off">
                                                                                        <div class="col mr-2">
                                                                                                
                                                                                                <a href="#" id="closeboxx" class="btn btn-primary" >Cerrar Caja</a>
                                                                                        </div>
                                                                                </form>

                                                                                <?php }?>
                                                                                        
                                                                </div>
                                                        </div>
                                                </div>
                                                    
                                        </div>

                                </div>
                                
                                

                        </div>
                </div>



            </div>
        </div>

    

</div>


</div>
<!-- End of Main Content -->
<?php include_once "includes/footer.php"; ?>