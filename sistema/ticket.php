<?php 
//ob_start();
require_once "model/Ticket.php";
require_once "model/Sales.php";
require_once "fpdf181/fpdf.php";
if(!empty($_GET['f'])){
    $codF= $_GET['f'];

    // CONFIGURACIÓN PREVIA

    define('EURO',chr(128));
    $pdf = new FPDF('P','mm',array(80,150));
    $pdf->AddPage();

    $objTik= new Ticket();
    $objsales= new Sales();
    $Phrama = $objTik->getPharmacyDetail();

    //value
    $prid='';
    $prname='';
    $prdescrip='';
    $prphone='';
    $premail='';
    $prruc='';
    $praddress='';
    $prigv='';

    //sales
    $stype_emision='';
    $stype_pyment='';
    $sseller='';
    $ssale_date='';
    $ssale_time='';
    $total_sale='';

    //salesDetail

    if($Phrama>0){
        foreach($Phrama  as $phr){
            $prid=$phr[0];
            $prname=$phr[1];
            $prdescrip=$phr[2];
            $prphone=$phr[3];
            $premail=$phr[4];
            $prruc=$phr[5];
            $praddress=$phr[6];
            $prigv=$phr[8];

        }

    } 

    $sales=$objsales->findSalesByID($codF);
    $salesDeatail=$objsales->findSalesDetailByID($codF);

    if($sales>0){
        foreach($sales as $sls){
            $stype_emision=$sls[1];
            $stype_pyment=$sls[2];
            $sseller=$sls[3];
            $ssale_date=$sls[4];
            $ssale_time=$sls[5];
            $total_sale=$sls[6];

        }

    }

    
    // CABECERA

    $pdf->Image('img/logo_v1.jpeg',25,8,30);
    $pdf->Ln(25);

    $pdf->SetFont('Helvetica','',12);
    $pdf->Cell(60,4,$prname,0,1,'C');
    $pdf->SetFont('Helvetica','',8);
    $pdf->Cell(60,4,'RUC:'.$prruc.'',0,1,'C');
    //$pdf->Cell(60,4,'C/ Arturo Soria, 1',0,1,'C');
    $pdf->Cell(60,4,$praddress,0,1,'C');
    $pdf->Cell(60,4,$prphone,0,1,'C');
    $pdf->Cell(60,4,$premail,0,1,'C');
    
    
    // DATOS FACTURA        
    $pdf->Ln(5);
    $pdf->Cell(60,4,'Factura Simpl.: '.$codF.'',0,1,'');
    $pdf->Cell(60,4,'Fecha: '.$ssale_date.'',0,1,'');
    $pdf->Cell(60,4,'Hora: '.$ssale_time.'',0,1,'');
    $pdf->Cell(60,4,'Metodo de pago: '.$stype_pyment.'',0,1,'');
    $pdf->Cell(60,4,'Vendido por: '.$sseller.'',0,1,'');

    
    // COLUMNAS
    $pdf->SetFont('Helvetica', 'B', 7);
    $pdf->Cell(30, 10, 'Producto', 0);
    $pdf->Cell(5, 10, 'Ud',0,0,'R');
    $pdf->Cell(10, 10, 'Precio',0,0,'R');
    $pdf->Cell(15, 10, 'Total',0,0,'R');
    $pdf->Ln(8);
    $pdf->Cell(60,0,'','T');
    $pdf->Ln(3);
    
    $i=1;
    if($salesDeatail>0){
        foreach($salesDeatail as $sldtl){

            $pdf->SetFont('Helvetica', '', 7);
            $pdf->MultiCell(30,4,$sldtl[1],0,'L'); 
            $pdf->Cell(35, -5, $sldtl[2],0,0,'R');
            $pdf->Cell(10, -5, number_format(round($sldtl[3],2), 2, ',', ' ') ,0,'R');
            $pdf->Cell(15, -5, number_format(round($sldtl[2]*$sldtl[3],2), 2, ',', ' '),0,0,'R');
            if($i<count($salesDeatail)){
                $pdf->Ln(3);
                
            }
            $i++;
            

        }

    }
    // PRODUCTOS
   /*
    $pdf->SetFont('Helvetica', '', 7);
    $pdf->MultiCell(30,4,$name,0,'L'); 
    $pdf->Cell(35, -5, '2',0,0,'R');
    $pdf->Cell(10, -5, 'S/ 2.00',0,0,'R');
    $pdf->Cell(10, -5, number_format(round(3,2), 2, ',', ' ') ,0,'R');
    $pdf->Cell(15, -5, number_format(round(2*3,2), 2, ',', ' '),0,0,'R');
    $pdf->Ln(3);
    $pdf->MultiCell(30,4,'Malla naranjas 3Kg',0,'L'); 
    $pdf->Cell(35, -5, '1',0,0,'R');
    $pdf->Cell(10, -5, number_format(round(1.25,2), 2, ',', ' ').EURO,0,0,'R');
    $pdf->Cell(15, -5, number_format(round(1.25,2), 2, ',', ' ').EURO,0,0,'R');
    $pdf->Ln(3);
    $pdf->MultiCell(30,4,'Uvas',0,'L'); 
    $pdf->Cell(35, -5, '5',0,0,'R');
    $pdf->Cell(10, -5, number_format(round(1,2), 2, ',', ' ').EURO,0,0,'R');
    $pdf->Cell(15, -5, number_format(round(1*5,2), 2, ',', ' ').EURO,0,0,'R');
    $pdf->Ln(3); */
    
    // SUMATORIO DE LOS PRODUCTOS Y EL IVA
    
    $pdf->Ln(6);
    $pdf->Cell(60,0,'','T');
    $pdf->Ln(2); /*   
    $pdf->Cell(25, 10, 'TOTAL SIN I.V.A.', 0);    
    $pdf->Cell(20, 10, '', 0);
    $pdf->Cell(15, 10, number_format(round((round(12.25,2)/1.21),2), 2, ',', ' ').EURO,0,0,'R');
    $pdf->Ln(3);    
    $pdf->Cell(25, 10, 'I.V.A. 21%', 0);    
    $pdf->Cell(20, 10, '', 0);
    $pdf->Cell(15, 10, number_format(round((round(12.25,2)),2)-round((round(2*3,2)/1.21),2), 2, ',', ' ').EURO,0,0,'R');
    $pdf->Ln(3);  */  
    $pdf->Cell(25, 10, 'TOTAL', 0);    
    $pdf->Cell(20, 10, '', 0);
    $pdf->Cell(15, 10, 'S/ '.$total_sale.'',0,0,'R');
    
    // PIE DE PAGINA
    $pdf->Ln(10);
    $pdf->Cell(60,0,'GRACIAS POR SU COMPRA',0,1,'C');
    

    $pdf->Output('ticket.pdf','i');
    //ob_end_flush(); 
}else{
    echo "error" ;
}

?>

