<?php include_once "includes/header.php"; ?>

<!-- Begin Page Content -->
<div class="container-fluid">

	<!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800">Proveedores</h1>
	</div>

	<div class="card shadow mb-4">
		<div class="card-header py-3">
							<h6 class="m-0 font-weight-bold text-primary float-left">Lista de proveedores</h6>
							<a href="registro_proveedor.php" class="float-right  btn btn-primary">Nuevo</a>
					
         </div>
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-sm table-bordered" id="table" width="100%" cellspacing="0">
					<thead class="table" id="head">
						<tr style="font-size: 14px;">
							<th>Id</th>
							<th>Ruc</th>
							<th>Proveedor</th>
							<th>Teléfono</th>
							<th>Dirección</th>
							<th>Contacto</th>
							<?php if ($_SESSION['rol'] == 1) { ?>
							<th>Aacciones</th>
							<?php } ?>
						</tr>
					</thead>
					<tbody>
						<?php
						require_once "controller/providerController.php";

						$provider = new ProviderController();
						$listprov = $provider->listProviderController();

						if ($listprov > 0) {
							foreach ($listprov as $prov) { ?>
								<tr style="background: white; font-size: 13px;">
									<td id="tr"><?php echo $prov[0]; ?></td>
									<td id="tr"><?php echo $prov[3]; ?></td>
									<td id="tr"><?php echo $prov[1]; ?></td>
									<td id="tr"><?php echo $prov[4]; ?></td>
									<td id="tr"><?php echo $prov[2]; ?></td>
									<td id="tr"><?php echo $prov[5]; ?></td>
									<?php if ($_SESSION['rol'] == 1) { ?>
									<td>

									<div class="row">
										<div style="display: inline-block;">
											<a href="editar_proveedor.php?id=<?php echo $prov[0];?>" class="btn btn-success"><i class='fas fa-edit'></i></a>
										</div>
										<div style="display: inline-block; margin-left: 2px;">
											<form action="option/deleteProvider.php?id=<?php echo $prov[0]; ?>" method="post" class="confirmar d-inline">
												<button class="btn btn-danger" type="submit"><i class='fas fa-trash-alt'></i> </button>
											</form>
										</div>
									</td>
									<?php } ?>
								</tr>
						<?php }
						} ?>
					</tbody>

				</table>
			</div>

		</div>
	</div>


</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->


<?php include_once "includes/footer.php"; ?>