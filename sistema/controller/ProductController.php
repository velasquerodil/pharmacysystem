
<?php
require_once "model/Product.php";
class ProductController{

    public function listProductController(){
        try{   
           $obj=new Product();
           return $obj->listProducts();
        }catch(Exception $e){
            throw $e;
        }
    }

    public function getProductsController($option){
        try{   
            $obj=new Product();
            return $obj->getProducts($option);
         }catch(Exception $e){
             throw $e;
         }
    }

    public function deleteProductController($idproduc){
        try{   
            $obj=new Product();
            return $obj->deleteProduct($idproduc);
         }catch(Exception $e){
             throw $e;
         }

    }

    public function saveProductController($option,$idprovider,$iduser,$barcode,$name,$brand,$presentation,$purchaseprice,$salesunit,$salesprice,$expiration,$stock,$acti){
        try{   
            $obj=new Product();
            return $obj->saveProduct($option,$idprovider,$iduser,$barcode,$name,$brand,$presentation,$purchaseprice,$salesunit,$salesprice,$expiration,$stock,$acti);
         }catch(Exception $e){
             throw $e;
         }

    }

    public function updateProductController($option,$idproduc,$idprovider,$iduser,$barcode,$name,$brand,$presentation,$purchaseprice,$salesunit,$salesprice,$expiration,$stock){
        try{   
            $obj=new Product();
            return $obj->updateProduct($option,$idproduc,$idprovider,$iduser,$barcode,$name,$brand,$presentation,$purchaseprice,$salesunit,$salesprice,$expiration,$stock);
         }catch(Exception $e){
             throw $e;
         }

    }

    public function getSalesUnitController(){
        try{   
            $obj=new Product();
            return $obj->getSalesUnit();
         }catch(Exception $e){
             throw $e;
         }

    }

    public function findSalesUnitByIdController($id){
        try{   
            $obj=new Product();
            return $obj->findSalesUnitById($id);
         }catch(Exception $e){
             throw $e;
         }

    }

    public function findProductbyIdController($id){
        try{   
            $obj=new Product();
            return $obj->findProductbyId($id);
         }catch(Exception $e){
             throw $e;
         }

    }

    public function getProductToExpireController(){
        try{   
            $obj=new Product();
            return $obj->getProductToExpire();
         }catch(Exception $e){
             throw $e;
         }

    }

    public function verifyBarCodeController($barcode){
        try{   
            $obj=new Product();
            return $obj->verifyBarCode($barcode);
         }catch(Exception $e){
             throw $e;
         }

    }




    }

 ?>   