<?php
require_once "model/Dashboard.php";
class DashboardController{

    public function listDashboardDataController(){
        try{   
           $obj=new Dashboard();
           return $obj->listDashboardData();
        }catch(Exception $e){
            throw $e;
        }
    }

    public function getLastSalesController(){
        try{   
            $obj=new Dashboard();
            return $obj->getLastSales();
         }catch(Exception $e){
             throw $e;
         }

    }

    public function getProductBestSellerController(){
        try{   
            $obj=new Dashboard();
            return $obj->getProductBestSeller();
         }catch(Exception $e){
             throw $e;
         }

    }

}  