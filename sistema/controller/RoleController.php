<?php
require_once "model/Role.php";
class RoleController{

    public function listRolesController(){
        try{   
           $obj=new Role();
           return $obj->listRoles();
        }catch(Exception $e){
            throw $e;
        }
       }

    public function getlastRolController(){
        try{   
            $obj=new Role();
            return $obj->getlastRol();
         }catch(Exception $e){
             throw $e;
         }
    }
    public function saveRolController($nombre,$descr,$act){
        try{   
            $obj=new Role();
            return $obj->saveRol($nombre,$descr,$act);
         }catch(Exception $e){
             throw $e;
            }
    }  
    
    public function verifyRolController($nombre){
        try{   
            $obj=new Role();
            return $obj->verifyRol($nombre);
         }catch(Exception $e){
             throw $e;
            }

    }

    public function getRolController($idrol){
        try{   
            $obj=new Role();
            return $obj->getRol($idrol);
         }catch(Exception $e){
             throw $e;
            }

    }

    public function editRolController($name,$desc, $act,$idrol){
        try{   
            $obj=new Role();
            return $obj->editRol($name,$desc, $act,$idrol);
         }catch(Exception $e){
             throw $e;
            }

    }

}  