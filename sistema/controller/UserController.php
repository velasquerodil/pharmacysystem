
<?php

require_once  "model/User.php";

class UserController{

    public function listUsersController(){
        try{   
           $obj=new User();
           return $obj->listUsers();
        }catch(Exception $e){
            throw $e;
        }
    }

    public function deleteUserController($id){
        try{   
            $obj=new User();
            return $obj->deleteUser($id);
         }catch(Exception $e){
             throw $e;
         }
    }
    
    
    public function verifyEmailController($email){
        try{   
            $obj=new User();
            return $obj->verifyEmail($email);
         }catch(Exception $e){
             throw $e;
         }

    }

    public function saveUserController($option,$idrol,$name,$surname,$email,$password, $active){
        try{   
            $obj=new User();
            return $obj->saveUser($option,$idrol,$name,$surname,$email,$password, $active);
         }catch(Exception $e){
             throw $e;
         }

    }

    public function editUserController($option,$iduser,$idrol,$name,$surname,$email){
        try{   
            $obj=new User();
            return $obj->editUser($option,$iduser,$idrol,$name,$surname,$email);
         }catch(Exception $e){
             throw $e;
         }

    }

    public function getuserByIdController($iduser){
        try{   
            $obj=new User();
            return $obj->getuserByID($iduser);
         }catch(Exception $e){
             throw $e;
         }

    }

    public function startUserSessionController($email, $pass){
        try{   
            $obj=new User();
            return $obj->startUserSession($email, $pass);
         }catch(Exception $e){
             throw $e;
         }

    }

} 
    
    


?>