<?php
require_once "model/Provider.php";
class ProviderController{

    public function deleteProviderController($id){
        try{   
        $obj=new Provider();
        return $obj->deleteProvider($id);
        }catch(Exception $e){
            throw $e;
        }
    }

    public function listProviderController(){
        try{   
        $obj=new Provider();
        return $obj->listProvider();
        }catch(Exception $e){
            throw $e;
        }
    }

    public function saveProviderController($option,$name,$address,$ruc,$phone,$person_contact,$activo){
        try{   
        $obj=new Provider();
        return $obj->saveProvider($option,$name,$address,$ruc,$phone,$person_contact,$activo);
        }catch(Exception $e){
            throw $e;
        }
    }


    public function updateProviderController($option,$id_provider,$name,$address,$ruc,$phone,$person_contact){
        try{   
        $obj=new Provider();
        return $obj->updateProvider($option,$id_provider,$name,$address,$ruc,$phone,$person_contact);
        }catch(Exception $e){
            throw $e;
        }
    }

    public function verifyRucController($ruc){
        try{   
            $obj=new Provider();
            return $obj->verifyRuc($ruc);
            }catch(Exception $e){
                throw $e;
        }

    }

    public function findProviderByIDController($id){
        try{   
            $obj=new Provider();
            return $obj->findProviderByID($id);
            }catch(Exception $e){
                throw $e;
        }

    }



}

 ?>   