
<?php
require_once "model/Client.php";
class ClientController{

    public function listClientesController(){
        try{   
           $obj=new Client();
           return $obj->listClients();
        }catch(Exception $e){
            throw $e;
        }
    }

    public function deleteClientController($id){
        try{   
            $obj=new Client();
            return $obj->deleteClient($id);
         }catch(Exception $e){
             throw $e;
         }
    }

    public function updateClientController($dni,$nombre,$telefono,$direccion,$idcliente){
        try{   
            $obj=new Client();
            return $obj->updateClient($dni,$nombre,$telefono,$direccion,$idcliente);
         }catch(Exception $e){
             throw $e;
         }

    }

    public function saveClientController($dni,$nombre,$telefono,$direccion,$iduser){
        try{   
            $obj=new Client();
            return $obj->saveClient($dni,$nombre,$telefono,$direccion,$iduser);
         }catch(Exception $e){
             throw $e;
         }

    }

    public function findClientByDniController($dni){
        try{   
            $obj=new Client();
            return $obj->findClientByDni($dni);
         }catch(Exception $e){
             throw $e;
         }

    }

    public function findClientByIdController($id){
        try{   
            $obj=new Client();
            return $obj->findClientByID($id);
         }catch(Exception $e){
             throw $e;
         }

    }

}   


