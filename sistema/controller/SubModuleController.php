<?php
require_once "model/SubModule.php";
class SubModuleController{

    public function listSubModuleController($idMod){
        try{   
           $obj=new SubModule();
           return $obj->listSubModules($idMod);
        }catch(Exception $e){
            throw $e;
        }
       }

}  