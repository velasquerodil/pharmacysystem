

<?php
require_once "model/Module.php";
class ModuleController{

    public function listModuleController($idRol){
        try{   
           $obj=new Module();
           return $obj->listModules($idRol);
        }catch(Exception $e){
            throw $e;
        }
       }

       public function saveAccesModuleController($idrols,$idmoduleType,$idmodule,$active){
        try{   
           $obj=new Module();
           return $obj->saveAccesModule($idrols,$idmoduleType,$idmodule,$active);
        }catch(Exception $e){
            throw $e;
        }
       }

       public function editAccesModuleController($active,$idmodule,$idrols){
            try{   
                $obj=new Module();
                return $obj->editAccesModule($active,$idmodule,$idrols);
            }catch(Exception $e){
                throw $e;
            }

       }

       public function listActiveModulesByRolController($idRol,$active){
        try{   
            $obj=new Module();
            return $obj->listActiveModulesByRol($idRol,$active);
         }catch(Exception $e){
             throw $e;
         }

       }
    }  