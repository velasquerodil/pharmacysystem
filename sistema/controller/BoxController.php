
<?php
require_once "model/Box.php";
class BoxController{

    public function openBoxController($iduser, $initial_balance){
        try{   
           $obj=new Box();
           return $obj->openBox($iduser, $initial_balance);
        }catch(Exception $e){
            throw $e;
        }
    }

    public function closeBoxController(){
        try{   
            $obj=new Box();
            return $obj->closeBox();
         }catch(Exception $e){
             throw $e;
         }

    }

    public function sumBalanceController($paga){
        try{   
            $obj=new Box();
            return $obj->sumBalance($paga);
         }catch(Exception $e){
             throw $e;
         }

    }

    public function substractBalanceController($vuelto){
        try{   
            $obj=new Box();
            return $obj->substractBalance($vuelto);
         }catch(Exception $e){
             throw $e;
         }

    }

    public function getBoxDataController($date){
        try{   
            $obj=new Box();
            return $obj->getBoxData($date);
         }catch(Exception $e){
             throw $e;
         }
    }

}  

?>