<?php
require_once "model/Sales.php";
class SalesController{

    public function listSalesController(){
        try{   
           $obj=new Sales();
           return $obj->listSales();
        }catch(Exception $e){
            throw $e;
        }
    }
    public function getTipePymentController(){

        try{   
            $obj=new Sales();
            return $obj->getTipePyment();
         }catch(Exception $e){
             throw $e;
         }
    }

    public function verifyBoxController(){
        try{   
            $obj=new Sales();
            return $obj->verifyBox();
         }catch(Exception $e){
             throw $e;
         }

    }

}  

?>