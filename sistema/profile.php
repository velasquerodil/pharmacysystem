<?php 
include_once "includes/header.php";

?>
<style>
.profile-head {
    transform: translateY(5rem)
}

.cover {
    background-image: linear-gradient(to right top, #051937, #004d7a, #008793, #00bf72, #a8eb12);
    background-size: cover;
    background-repeat: no-repeat
}

body {
    background: #654ea3;
    background: linear-gradient(to right, #e96443, #904e95);
    min-height: 100vh
}

</style>

<!--Begin Page Content-->
<div class="container-fluid">
        
            <!-- Profile widget -->
            <div class="bg-white shadow rounded overflow-hidden">
                <div class="px-4 pt-0 pb-4 cover">
                    <div class="media align-items-end profile-head">
                        <div class="profile mr-3"><img src="img/photo.jpg" alt="..." width="130" class="rounded mb-2 img-thumbnail">
                        
                        <!--<a href="#" class="btn btn-outline-dark btn-sm btn-block">Edit profile</a> -->
                    </div>
                        <div class="media-body mb-5 text-white">
                            <h4 class="mt-0 mb-0"><?php echo $_SESSION['nombre']." ".$_SESSION['surname'] ?></h4>
                            <p class="small mb-0"> <i class="fas fas fa-stream mr-2"></i><?php echo $_SESSION['email'] ?></p>
                            <p class="small mb-4"> <i class="fas fa-shield-alt mr-2"></i><?php echo $_SESSION['rol_name'] ?></p>
                        </div>
                    </div>
                </div>
                <div class="bg-light p-4 d-flex justify-content-end text-center">
                    <ul class="list-inline mb-0">
                        <li class="list-inline-item">
                            <h5 class="font-weight-bold mb-0 d-block">●</h5><small class="text-muted"> <i ></i>Ventas</small>
                        </li>
                        <li class="list-inline-item">
                            <h5 class="font-weight-bold mb-0 d-block">●</h5><small class="text-muted"> <i ></i>Productos</small>
                        </li>
                        <li class="list-inline-item">
                            <h5 class="font-weight-bold mb-0 d-block">●</h5><small class="text-muted"> <i ></i>Clientes</small>
                        </li>
                    </ul>
                </div>
                <div class="px-4 py-3">
                    <h5 class="mb-0">Acerca de</h5>
                    <div class="p-4 rounded shadow-sm bg-light">
                        <p class="font-italic mb-0">por definir</p>
                      
                    </div>
                </div>
        
            </div>
       
   

</div>


</div>
<!-- End of Main Content -->
<?php include_once "includes/footer.php"; ?>