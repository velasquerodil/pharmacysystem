<?php 
include_once "includes/header.php";

require_once "controller/UserController.php";
require_once "controller/RoleController.php";
if (!empty($_POST)) {
    $alert = "";
    if (empty($_POST['nombre']) || empty($_POST['correo']) || empty($_POST['apellidos']) || empty($_POST['clave']) || empty($_POST['rol'])) {
        $alert = '<div class="alert alert-primary" role="alert">
                    Todo los campos son obligatorios
                </div>';
    } else {

    
        $nombre = $_POST['nombre'];
        $email = $_POST['correo'];
        $apellidos = $_POST['apellidos'];
        $clave = base64_encode($_POST['clave']);
        //md5();
        $rol = $_POST['rol'];
        $option = "saveUser";

        $UserCon = new UserController();
        //$option,$idrol,$name,$surname,$email,$password, $active
        $isEmail = $UserCon->verifyEmailController($email);	
        if (!empty($isEmail) > 0) {
            $alert = '<div class="alert alert-danger" role="alert">
                        El correo ya existe
                    </div>';
        } else {
            $query_insert = $UserCon->saveUserController($option,$rol,$nombre,$apellidos,$email,$clave,1);
            if ($query_insert) {
                $alert = '<div class="alert alert-primary" role="alert" style="background: #4EDCBC;">
                            Usuario registrado
                        </div>';
            } else {
                $alert = '<div class="alert alert-danger" role="alert">
                        Error al registrar
                    </div>';
            }
        }
    }
}
?>

<!--Begin Page Content-->
<div class="container-fluid">
    <!-- Content Row -->
    <div class="row">
        <div class="col-lg-6 m-auto">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <div class="d-sm-flex align-items-center justify-content-between">
                    <h5 class="m-0 font-weight-bold text-primary">Registro Usuario</h5>
                        <a href="lista_usuarios.php" class="btn btn-primary">Regresar</a>
                    </div>
                </div>
                <div class="card-body">
                    <form action="" method="post" autocomplete="off" class="card-body p-3">
                        <?php echo isset($alert) ? $alert : ''; ?>
                        <div class="form-group">
                            <label for="nombre">Nombre</label>
                            <input type="text" class="form-control" placeholder="Ingrese Nombre" name="nombre" id="nombre" required> 
                        </div>
                        <div class="form-group">
                            <label for="apellidos">Apellidos</label>
                            <input type="text" class="form-control" placeholder="Ingrese Apellidos" name="apellidos" id="apellidos" required>
                        </div>
                        <div class="form-group">
                            <label for="correo">Correo</label>
                            <input type="email" class="form-control" placeholder="Ingrese Correo Electrónico" name="correo" id="correo" required>
                        </div>
                        <div class="form-group">
                            <label for="clave">Contraseña</label>
                            <input type="password" class="form-control" placeholder="Ingrese Contraseña" name="clave" id="clave" required>
                        </div>
                        <div class="form-group">
                            <label>Rol</label>
                            <select name="rol" id="rol" class="form-control">
                                <?php

                                $RolesObj = new RoleController();
                                $listRoles = $RolesObj->listRolesController();
                                if ($listRoles > 0) {
                                    foreach($listRoles as $rol) {
                                ?>
                                        <option value="<?php echo $rol[0]; ?>"><?php echo $rol[1] ?></option>
                                <?php

                                    }
                                }

                                ?>
                            </select></div>

                        <input type="submit" value="Guardar Usuario" class="btn btn-primary">

                    </form>

                </div>

            </div>

        </div>

    </div>
   
</div>
<!-- End of Main Content -->

</div>
<?php include_once "includes/footer.php"; ?>