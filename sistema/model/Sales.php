<?php

include_once "conexion.php";

class Sales{

    public function __construct() {
        $con = new Conexion();
    }
    
    public function listSales(){
        try{   
            $obj = Conexion::singleton();
            $query=$obj->prepare("SELECT s.id_sales, t.name as tipo_emision, p.name as tipo_pago, s.sale_date, s.time, s.total_sale FROM frm_sales s
            left join frm_type_sales t on t.id_type_sales =s.id_type_sales
            left join frm_type_pyment p on p.id_type_pyment =s.id_type_pyment");
             $query->execute();
             $lista = $query->fetchAll();
             $query=null;
             return $lista;	
         }catch(Exception $e){
             throw $e;
         }
    }

    public function findSalesByID($id){
        try{   
            $obj = Conexion::singleton();
            $query=$obj->prepare("SELECT s.id_sales, t.name as tipo_emision, p.name as tipo_pago, u.name as atentido_por, s.sale_date, s.time, s.total_sale FROM frm_sales s
            left join frm_type_sales t on t.id_type_sales =s.id_type_sales
            left join frm_type_pyment p on p.id_type_pyment =s.id_type_pyment
            left join frm_user u on u.id_user=s.id_user
            where s.id_sales=$id");
             $query->execute();
             $lista = $query->fetchAll();
             $query=null;
             return $lista;	
         }catch(Exception $e){
             throw $e;
         }

    }


    public function findSalesDetailByID($id){
        try{   
            $obj = Conexion::singleton();
            $query=$obj->prepare("SELECT d.id_sales, p.name, d.quantity, d.sale_price from frm_sales_detail d
            left join frm_product p on p.id_product = d.cod_product
            where d.id_sales=$id");
             $query->execute();
             $lista = $query->fetchAll();
             $query=null;
             return $lista;	
         }catch(Exception $e){
             throw $e;
         }

    }
    public function getTipePyment(){
        try{   
            $obj = Conexion::singleton();
            $query=$obj->prepare("SELECT * FROM frm_type_pyment");
             $query->execute();
             $lista = $query->fetchAll();
             $query=null;
             return $lista;	
         }catch(Exception $e){
             throw $e;
         }

    }

    public function verifyBox(){
        try{   
            $obj = Conexion::singleton();
            $query=$obj->prepare("SELECT is_open FROM frm_box where open_date = curdate()");
             $query->execute();
             $lista = $query->fetchAll();
             $query=null;
             return $lista;	
         }catch(Exception $e){
             throw $e;
         }

    }
}

?>