<?php

include_once "conexion.php";

class Box{

    public function __construct() {
        $con = new Conexion();
    }
    
    public function openBox($iduser, $initial_balance){
        $option = "openBox";
        $f_b=0;
        try{   
            $obj = Conexion::singleton();
            $query=$obj->prepare('Call frm_sp_box(?,?,?,?)');
            $query->bindParam(1,$option);
            $query->bindParam(2,$iduser);
            $query->bindParam(3,$initial_balance);
            $query->bindParam(4,$f_b);
            $query->execute();
            return $query;
         }catch(Exception $e){
             throw $e;
         }
    }

    public function closeBox(){
        $option = "closeBox";
        $i_b=0;
        $iduser=0;
        $f_b=0;
        try{   
            $obj = Conexion::singleton();
            $query=$obj->prepare('Call frm_sp_box(?,?,?,?)');
            $query->bindParam(1,$option);
            $query->bindParam(2,$iduser);
            $query->bindParam(3,$i_b);
            $query->bindParam(4,$f_b);
            $query->execute();
            return $query;
         }catch(Exception $e){
             throw $e;
         }
    }

   public function sumBalance($paga){
     
    try{   
        $obj = Conexion::singleton();
        $query=$obj->prepare("update frm_box set final_balance= (final_balance+$paga) where open_date= curdate() and is_open =1");
        $query->execute();	
     }catch(Exception $e){
         throw $e;
     }
   } 

   public function substractBalance($vuelto){
    try{   
        $obj = Conexion::singleton();
        $query=$obj->prepare("update frm_box set final_balance= (final_balance-$vuelto) where  open_date= curdate() and is_open =1");
        $query->execute();	
     }catch(Exception $e){
         throw $e;
     }

   }

   public function getBoxData($date){
  
    try{   
        $obj = Conexion::singleton();
        $query=$obj->prepare('Call frm_box_data(?)');
        $query->bindParam(1,$date);
        $query->execute();
        $lista = $query->fetchAll();
        $query=null;
        return $lista;	
     }catch(Exception $e){
         throw $e;
     }

   }

}

?>