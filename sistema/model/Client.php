<?php

include_once "conexion.php";

class Client{

    public function __construct() {
        $con = new Conexion();
    }
    

    public function listClients(){
        try{   
            $obj = Conexion::singleton();
            $query=$obj->prepare("SELECT * FROM frm_client");
            $query->execute();
            $lista = $query->fetchAll();
            $query=null;
            return $lista;	
         }catch(Exception $e){
             throw $e;
         }
    }

    public function deleteClient($id){
        try{   
            $obj = Conexion::singleton();
            $query=$obj->prepare("DELETE FROM frm_client WHERE idcliente = $id");
            $query->execute();
            return $query;
         }catch(Exception $e){
             throw $e;
         }
    }

    public function updateClient($dni,$nombre,$telefono,$direccion,$idcliente){    
        try{   
            $obj = Conexion::singleton();
            $query=$obj->prepare("UPDATE frm_client SET dni = $dni, nombre = '$nombre' , telefono = '$telefono', direccion = '$direccion' WHERE idcliente = $idcliente");
            $query->execute();
            return $query;
         }catch(Exception $e){
             throw $e;
         }
    }

    public function saveClient($dni,$nombre,$telefono,$direccion,$usuario_id){
        try{   
            $obj = Conexion::singleton();
            $query=$obj->prepare("INSERT INTO frm_client(dni,nombre,telefono,direccion, usuario_id) values ('$dni', '$nombre', '$telefono', '$direccion', '$usuario_id')");
            $query->execute();
            return $query;
         }catch(Exception $e){
             throw $e;
         }

    }

    public function findClientByDni($dni){
        try{   
            $obj = Conexion::singleton();
            $query=$obj->prepare("SELECT * FROM frm_client where dni = '$dni'");
            $query->execute();
            $lista = $query->fetchAll();
            $query=null;
            return $lista;			
         }catch(Exception $e){
             throw $e;
         }
    }
     public function findClientByID($id)
     {
         # 
         try{   
            $obj = Conexion::singleton();
            $query=$obj->prepare("SELECT * FROM frm_client WHERE idcliente = $id");
            $query->execute();
            $lista = $query->fetchAll();
            $query=null;
            return $lista;			
         }catch(Exception $e){
             throw $e;
         }
     }

    
}

?>