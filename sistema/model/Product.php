

<?php
include_once "conexion.php";
class Product
{
    public function __construct()
    {
        $con = new Conexion();
    }


    public function listProducts()
    {
        try {
            $obj = Conexion::singleton();
            $query = $obj->prepare("SELECT * FROM producto");
            $query->execute();
            $lista = $query->fetchAll();
            $query = null;
            return $lista;
        } catch (Exception $e) {
            throw $e;
        }
    }


    public function deleteProduct($codproduct)
    {
        try {
            $obj = Conexion::singleton();
            $query = $obj->prepare("DELETE FROM frm_product WHERE id_product = $codproduct");
            $query->execute();
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getProducts($option)
    {
        try {
            $idproduc = 0;
            $idprovider = 0;
            $iduser = 0;
            $barcode = "0";
            $name = "0";
            $brand = "0";
            $presentation = "0";
            $purchaseprice = 0.0;
            $salesunit = 0;
            $salesprice = 0.0;
            $expiration = 0;
            $stock = 0;
            $acti = 0;
            $obj = Conexion::singleton();
            $query = $obj->prepare('CALL frm_sp_product(?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $query->bindParam(1, $option);
            $query->bindParam(2, $idproduc);
            $query->bindParam(3, $idprovider);
            $query->bindParam(4, $iduser);
            $query->bindParam(5, $barcode);
            $query->bindParam(6, $name);
            $query->bindParam(7, $brand);
            $query->bindParam(8, $presentation);
            $query->bindParam(9, $purchaseprice);
            $query->bindParam(10, $salesunit);
            $query->bindParam(11, $salesprice);
            $query->bindParam(12, $expiration);
            $query->bindParam(13, $stock);
            $query->bindParam(14, $acti);
            $query->execute();
            $lista = $query->fetchAll();
            $query = null;
            return $lista;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function saveProduct($option, $idprovider, $iduser, $barcode, $name, $brand, $presentation, $purchaseprice, $salesunit, $salesprice, $expiration, $stock, $acti)
    {
        try {

            $idproduc = 0;
            $obj = Conexion::singleton();
            $query = $obj->prepare('CALL frm_sp_product(?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $query->bindParam(1, $option);
            $query->bindParam(2, $idproduc);
            $query->bindParam(3, $idprovider);
            $query->bindParam(4, $iduser);
            $query->bindParam(5, $barcode);
            $query->bindParam(6, $name);
            $query->bindParam(7, $brand);
            $query->bindParam(8, $presentation);
            $query->bindParam(9, $purchaseprice);
            $query->bindParam(10, $salesunit);
            $query->bindParam(11, $salesprice);
            $query->bindParam(12, $expiration);
            $query->bindParam(13, $stock);
            $query->bindParam(14, $acti);
            $query->execute();
            return $query;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function updateProduct($option, $idproduc, $idprovider, $iduser, $barcode, $name, $brand, $presentation, $purchaseprice, $salesunit, $salesprice, $expiration, $stock)
    {
        try {
            $acti = 0;
            $obj = Conexion::singleton();
            $query = $obj->prepare('CALL frm_sp_product(?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $query->bindParam(1, $option);
            $query->bindParam(2, $idproduc);
            $query->bindParam(3, $idprovider);
            $query->bindParam(4, $iduser);
            $query->bindParam(5, $barcode);
            $query->bindParam(6, $name);
            $query->bindParam(7, $brand);
            $query->bindParam(8, $presentation);
            $query->bindParam(9, $purchaseprice);
            $query->bindParam(10, $salesunit);
            $query->bindParam(11, $salesprice);
            $query->bindParam(12, $expiration);
            $query->bindParam(13, $stock);
            $query->bindParam(14, $acti);
            $query->execute();
            return $query;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getSalesUnit()
    {

        try {
            $obj = Conexion::singleton();
            $query = $obj->prepare("SELECT * FROM frm_sales_unit");
            $query->execute();
            $lista = $query->fetchAll();
            $query = null;
            return $lista;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function findSalesUnitById($id)
    {

        try {
            $obj = Conexion::singleton();
            $query = $obj->prepare("SELECT * FROM frm_sales_unit where id_sales_unit=$id");
            $query->execute();
            $lista = $query->fetchAll();
            $query = null;
            return $lista;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function findProductbyId($id)
    {

        try {
            $obj = Conexion::singleton();
            $query = $obj->prepare("select p.id_provider, p.bar_cod, p.name, p.brand, p.presentation, p.purchase_price, p.id_sales_unit,  p.stock, p.unit_sale_price,  p.expiration from frm_product p
            where p.id_product=$id");
            $query->execute();
            $lista = $query->fetchAll();
            $query = null;
            return $lista;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getProductToExpire()
    {
        try {
            $obj = Conexion::singleton();
            $query = $obj->prepare("select p.id_product,p.bar_cod, p.name, p.brand, p.presentation, s.name, p.stock, p.unit_sale_price, p.expiration, pr.name as Proveedor from frm_product p
            left join frm_sales_unit s on p.id_sales_unit=s.id_sales_unit
            left join frm_provider pr on p.id_provider=pr.id_provider 
            where CURDATE() >= DATE_SUB(p.expiration,INTERVAL 60 DAY)");
            $query->execute();
            $lista = $query->fetchAll();
            $query = null;
            return $lista;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function verifyBarCode($barcode){
        try{   
            $obj = Conexion::singleton();
            $query=$obj->prepare("SELECT * FROM frm_product where bar_cod = '$barcode'");
            $query->execute();
            $lista = $query->fetchAll();
            $query=null;
            return $lista;	
         }catch(Exception $e){
             throw $e;
         }

    }
}
?>