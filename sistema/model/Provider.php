<?php

include_once "conexion.php";

class Provider{

    public function __construct() {
        $con = new Conexion();
    }
    
    public function deleteProvider($id){
        try{   
            $obj = Conexion::singleton();
            $query=$obj->prepare("DELETE FROM frm_provider WHERE id_provider = $id");
            $query->execute();	
            return $query;
         }catch(Exception $e){
             throw $e;
         }
    }

    public function listProvider(){
        try{   
            $obj = Conexion::singleton();
            $query=$obj->prepare("SELECT * FROM frm_provider");
            $query->execute();
            $lista = $query->fetchAll();
            $query=null;
            return $lista;		
         }catch(Exception $e){
             throw $e;
         }
    }

    public function saveProvider($option,$name,$address,$ruc,$phone,$person_contact,$activo){
        try{  

            $id_provider=0;
            $obj = Conexion::singleton();
            $query = $obj->prepare('CALL frm_sp_provider(?,?,?,?,?,?,?,?)');
            $query->bindParam(1,$option);
            $query->bindParam(2,$id_provider);
            $query->bindParam(3,$name);
            $query->bindParam(4,$address);
            $query->bindParam(5,$ruc);
            $query->bindParam(6,$phone);
            $query->bindParam(7,$person_contact);
            $query->bindParam(8,$activo);
            $query->execute();
            return $query;
         }catch(Exception $e){
             throw $e;
         }

    }

    public function updateProvider($option,$id_provider,$name,$address,$ruc,$phone,$person_contact){
        try{   
            $act=1;
            $obj = Conexion::singleton();
            $query = $obj->prepare('CALL frm_sp_provider(?,?,?,?,?,?,?,?)');
            $query->bindParam(1,$option);
            $query->bindParam(2,$id_provider);
            $query->bindParam(3,$name);
            $query->bindParam(4,$address);
            $query->bindParam(5,$ruc);
            $query->bindParam(6,$phone);
            $query->bindParam(7,$person_contact);
            $query->bindParam(8,$act);
            $query->execute();
            return $query;
         }catch(Exception $e){
             throw $e;
         }
        
    }
    public function verifyRuc($ruc){
        try{   
            $obj = Conexion::singleton();
            $query=$obj->prepare("SELECT * FROM frm_provider where ruc= '$ruc'");
            $query->execute();
            $lista = $query->fetchAll();
            $query=null;
            return $lista;	
         }catch(Exception $e){
             throw $e;
         }

    }

    public function findProviderByID($id){
        try{   
            $obj = Conexion::singleton();
            $query=$obj->prepare("SELECT * FROM frm_provider where id_provider = $id");
            $query->execute();
            $lista = $query->fetchAll();
            $query=null;
            return $lista;	
         }catch(Exception $e){
            throw $e;
         }

    }

    
}

?>