
<?php

include_once "conexion.php";
class Dashboard{

    public function __construct() {
        $con = new Conexion();
    }


    public function listDashboardData(){
        try{   
            $obj = Conexion::singleton();
            $query=$obj->prepare('CALL data()');
            $query->execute();
            $lista = $query->fetchAll();
            $query=null;
            return $lista;	
         }catch(Exception $e){
             throw $e;
         }
    }

    public function getLastSales(){
        try{   
            $obj = Conexion::singleton();
            $query=$obj->prepare('SELECT s.id_sales, t.name as tipo_emision, p.name as tipo_pago, u.name as atentido_por, s.sale_date, s.time, s.total_sale FROM frm_sales s
                                left join frm_type_sales t on t.id_type_sales =s.id_type_sales
                                left join frm_type_pyment p on p.id_type_pyment =s.id_type_pyment
                                left join frm_user u on u.id_user=s.id_user
                                order by s.id_sales DESC
                                LIMIT 5');
            $query->execute();
            $lista = $query->fetchAll();
            $query=null;
            return $lista;	
        }catch(Exception $e){
            throw $e;
        }
    }

    public function getProductBestSeller(){
        try{   
            $obj = Conexion::singleton();
            $query=$obj->prepare('SELECT tp.cod_product, COUNT(1) AS veces, p.name, ROUND(sum(tp.quantity * tp.sale_price),2)
                        FROM frm_sales_detail tp
                        left join frm_product p on p.id_product = tp.cod_product
                        GROUP BY tp.cod_product
                        HAVING COUNT(1) >= 1
                        order by veces desc
                        limit 5');
            $query->execute();
            $lista = $query->fetchAll();
            $query=null;
            return $lista;	
        }catch(Exception $e){
            throw $e;
        }

    }



 
}
?>