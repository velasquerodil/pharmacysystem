
<?php

include_once "conexion.php";

class User{

    public function __construct() {
        $con = new Conexion();
    }
    

    public function listUsers(){
        try{   
            $obj = Conexion::singleton();
            $query=$obj->prepare("SELECT u.id_user, u.name, u.surname, u.email,u.created_at, u.update_at, r.name, u.password  FROM frm_user u INNER JOIN frm_rols r ON u.idapp_rols = r.id_rols");
            $query->execute();
            $lista = $query->fetchAll();
            $query=null;
            return $lista;	
         }catch(Exception $e){
             throw $e;
         }
    }

    public function deleteUser($id){
        try{   
            $obj = Conexion::singleton();
            $query=$obj->prepare("DELETE FROM frm_user WHERE id_user = $id");
            $query->execute();	
         }catch(Exception $e){
             throw $e;
         }
    }


    public function verifyEmail($email){
        try{   
            $obj = Conexion::singleton();
            $query=$obj->prepare("SELECT * FROM frm_user where email = '$email'");
            $query->execute();
            $lista = $query->fetchAll();
            $query=null;
            return $lista;	
         }catch(Exception $e){
             throw $e;
         }
    }

    public function saveUser($option,$idrol,$name,$surname,$email,$password, $active){
        
            try{   
               $act=1;
               $obj = Conexion::singleton();
               $query = $obj->prepare('CALL frm_sp_user(?,?,?,?,?,?,?,?)');
               $query->bindParam(1,$option);
               $query->bindParam(2,$act);
               $query->bindParam(3,$idrol);
               $query->bindParam(4,$name);
               $query->bindParam(5,$surname);
               $query->bindParam(6,$email);
               $query->bindParam(7,$password);
               $query->bindParam(8,$active);
               $query->execute();
               return $query;
            }catch(Exception $e){
                throw $e;
            }
            
    }

    public function editUser($option,$iduser,$idrol,$name,$surname,$email){
        try{   
            $pass="cc";
            $act=1;
            $obj = Conexion::singleton();
            $query = $obj->prepare('CALL frm_sp_user(?,?,?,?,?,?,?,?)');
            $query->bindParam(1,$option);
            $query->bindParam(2,$iduser);
            $query->bindParam(3,$idrol);
            $query->bindParam(4,$name);
            $query->bindParam(5,$surname);
            $query->bindParam(6,$email);
            $query->bindParam(7,$pass);
            $query->bindParam(8,$act);
            $query->execute();
            return $query;
         }catch(Exception $e){
             throw $e;
         }
        
    }

    public function getuserByID($iduser){

        try{   
            $obj = Conexion::singleton();
            $query = $obj->prepare("SELECT u.id_user, u.name, u.surname, u.email, r.name as rol, u.password FROM frm_user u INNER JOIN frm_rols r ON u.idapp_rols = r.id_rols where u.id_user=$iduser");
            $query->execute();
            $lista = $query->fetchAll();
            $query=null;
            return $lista;	
         }catch(Exception $e){
             throw $e;
         }
    }

    public function startUserSession($email, $pass){
        try{   
            $obj = Conexion::singleton();
            $query = $obj->prepare("SELECT u.id_user, u.name, u.email ,u.surname, r.id_rols,r.name FROM frm_user u INNER JOIN frm_rols r ON u.idapp_rols = r.id_rols where u.email = '$email' and u.password ='$pass'");
            $query->execute();
            $lista = $query->fetchAll();
            $query=null;
            return $lista;	
         }catch(Exception $e){
             throw $e;
         }
        
    }

    
}

?>