

<?php

include_once "conexion.php";
class Module{

    public function __construct() {
        $con = new Conexion();
    }


    public function listModules($idRol){
        try{   
            $obj = Conexion::singleton();
            $query=$obj->prepare("SELECT a.id_module_main, a.id_rols,a.id_module_type,a.id_module, b.name, b.title, b.description, b.icon, b.labelledby  FROM frm_module_main_transaction a 
            left join frm_module b on a.id_module=b.id_module where a.id_rols = $idRol");
            $query->execute();
            $lista = $query->fetchAll();
            $query=null;
            return $lista;	
         }catch(Exception $e){
             throw $e;
         }
    }

    public function listActiveModulesByRol($idRol, $active){
        try{   
            $obj = Conexion::singleton();
            $query=$obj->prepare("SELECT a.id_module_main, a.id_rols,a.id_module_type,a.id_module, b.name, b.title, b.description, b.icon, b.labelledby  FROM frm_module_main_transaction a 
            left join frm_module b on a.id_module=b.id_module where a.id_rols = $idRol and a.active=$active");
            $query->execute();
            $lista = $query->fetchAll();
            $query=null;
            return $lista;	
         }catch(Exception $e){
             throw $e;
         }
    }


    public function  saveAccesModule($idrols,$idmoduleType,$idmodule,$active){
        try{   
            $obj = Conexion::singleton();
            $query = $obj->prepare("INSERT INTO frm_module_main_transaction(id_rols,id_module_type,id_module,active) values ('$idrols', '$idmoduleType', '$idmodule', '$active')");
            $query->execute();
            return $query;
         }catch(Exception $e){
             throw $e;
         }

    }

    public function  editAccesModule($active,$idmodule,$idrols){
        try{   
            $obj = Conexion::singleton();
            $query = $obj->prepare("UPDATE frm_module_main_transaction SET active=$active WHERE id_module=$idmodule and id_rols=$idrols");
            $query->execute();
            return $query;
         }catch(Exception $e){
             throw $e;
         }

    }

 
}
?>