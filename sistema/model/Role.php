

<?php

include_once "conexion.php";

class Role{

    public function __construct() {
        $con = new Conexion();
    }

    public function listRoles(){
        try{   
            $obj = Conexion::singleton();
            $query=$obj->prepare("SELECT * FROM frm_rols");
            $query->execute();
            $lista = $query->fetchAll();
            $query=null;
            return $lista;	
         }catch(Exception $e){
             throw $e;
         }

    }


    public function getlastRol(){
        try{   
            $obj = Conexion::singleton();
            $query=$obj->prepare("SELECT * FROM frm_rols ORDER BY id_rols DESC LIMIT 1");
            $query->execute();
            $lista = $query->fetchAll();
            $query=null;
            return $lista;	
         }catch(Exception $e){
             throw $e;
         }

    }

    public function saveRol($nombre,$descr,$act){
        try{   
            $obj = Conexion::singleton();
            $query=$obj->prepare("INSERT INTO frm_rols(name,description,active) VALUES ('$nombre', '$descr','$act')");
            $query->execute();
            return $query;	
         }catch(Exception $e){
             throw $e;
         }

    }

    public function verifyRol($name){
        try{   
            $obj = Conexion::singleton();
            $query=$obj->prepare("SELECT * FROM frm_rols WHERE name='$name'");
            $query->execute();
            $lista = $query->fetchAll();
            $query=null;
            return $lista;	
         }catch(Exception $e){
             throw $e;
         }
        
    }

    public function getRol($idrol){
        try{   
            $obj = Conexion::singleton();
            $query=$obj->prepare("SELECT * FROM frm_rols WHERE id_rols=$idrol");
            $query->execute();
            $lista = $query->fetchAll();
            $query=null;
            return $lista;	
         }catch(Exception $e){
             throw $e;
         }
        
    }

    public function editRol($name,$desc, $act,$idrol){
        try{   
            $obj = Conexion::singleton();
            $query=$obj->prepare("UPDATE frm_rols SET name='$name', description='$desc',active='$act' where id_rols=$idrol");
            $query->execute();
            return $query;	
         }catch(Exception $e){
             throw $e;
         }
        
    }
    

}