<?php
include "includes/header.php";
require_once "controller/CompanyController.php";
require_once "controller/TicketController.php";

if (!empty($_POST)) {
  $alert = "";
  if (empty($_POST['nombre']) || empty($_POST['correo']) || empty($_POST['description']) || empty($_POST['cel'])) {
    $alert = '<p class"error">Todo los campos son requeridos</p>';
  } else {

 
    $idl =$_POST['id']; 
    $namel = $_POST['nombre'];
    $descriptl= $_POST['description'];
    $phonel= $_POST['cel'];
    $emaill = $_POST['correo'];
    $rucl= $_POST['ruc'];
    $addressl= $_POST['direccion'];
    $representativel= $_POST['ceo'];
    $igvl= $_POST['igv'];
  


    $objComp= new CompanyController();
    $q = $objComp->updateCompanyController($idl, $namel, $descriptl, $phonel,$emaill,$rucl,$addressl,$representativel,$igvl);
    if($q){
        $alert = '<div class="alert alert-primary" role="alert" style="background: #4EDCBC;">
                Empresa Actualizado..!!
              </div>';

    }
    
  }
}

// Mostrar Datos
$objtiked= new TicketController();
$sql=$objtiked->getPharmacyDetailController();

$id =''; 
$name=''; 
$descript=''; 
$phone=''; 
$email=''; 
$ruc=''; 
$address=''; 
$representative=''; 
$igv=''; 

if ($sql>0) {
  foreach ($sql as $frm) {
    $id = $frm[0];
    $name=$frm[1];
    $descript=$frm[2];
    $phone=$frm[3];
    $email=$frm[4];
    $ruc=$frm[5];
    $address=$frm[6];
    $representative=$frm[7];
    $igv=$frm[8];
  }
}
?>


<!-- Begin Page Content -->
<div class="container-fluid">

  <div class="row">
    <div class="col-lg-8 m-auto">

      <div class="card shadow mb-4">
          <div class="card-header py-3">
                    <div class="d-sm-flex align-items-center justify-content-between">
                    <h5 class="m-0 font-weight-bold text-primary">Mi Empresa</h5>
                    </div>
            </div>
        <div class="card-body">
          <form class="" action="" method="post">
            <?php echo isset($alert) ? $alert : ''; ?>
            <input type="hidden" name="id" value="<?php echo $id; ?>">

            <div class="form-group col-sm-11">
              <label for="nombre">Nombre</label>
              <input type="text" placeholder="Ingrese nombre" class="form-control" name="nombre" id="nombre" value="<?php echo $name; ?>">
            </div>

            <div class="form-group col-sm-11">
              <label for="description">Descripción</label>
              <input type="text" placeholder="Ingrese descripción" class="form-control" name="description" id="description" value="<?php echo $descript; ?>">
            </div>

            <div class="form-group col-sm-11">
              <label for="direccion">Dirección</label>
              <input type="text" placeholder="Ingrese Dirección" class="form-control" name="direccion" id="direccion" value="<?php echo $address; ?>">
            </div>


            <div class="form-group col-sm-8">
              <label for="correo">Correo</label>
              <input type="text" placeholder="Ingrese correo" class="form-control" name="correo" id="correo" value="<?php echo $email; ?>">
            </div>
            
            <div class="form-group col-sm-8">
              <label for="cel">Celular ó Teléfono</label>
              <input type="text" placeholder="Ingrese Celular" class="form-control" name="cel" id="cel" value="<?php echo $phone; ?>">
            </div>

            
            

            <div class="form-group col-sm-8">
              <label for="ruc">Ruc</label>
              <input type="text" placeholder="Ingrese Ruc" class="form-control" name="ruc" id="ruc" value="<?php echo $ruc; ?>">
            </div>

            <div class="form-group col-sm-8">
              <label for="ceo">Representante</label>
              <input type="text" placeholder="Ingrese Dirección" class="form-control" name="ceo" id="ceo" value="<?php echo $representative; ?>">
            </div>

            <div class="form-group col-sm-4">
            <label for="ceo">Igv</label>
                <input type="number" step="any"  class="form-control" id="igv" name="igv" placeholder="00.00" value="<?php echo $igv; ?>">
            </div>


            
            <button type="submit" class="btn btn-primary"><i class="fas fa-vote-yea"></i> Actualizar Información</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
<?php include_once "includes/footer.php"; ?>