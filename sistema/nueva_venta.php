<?php include_once "includes/header.php";
require_once "controller/ProductController.php";
require_once "controller/SalesController.php";
include "model/zccn.php";

$detalleTabla = '';
$detalleTotales = '';





$token = md5($_SESSION['idUser']);
$query_iva = mysqli_query($conexion, "SELECT igv FROM fmr_pharmacy");
$result_iva = mysqli_num_rows($query_iva);
$query_detalle_temp = mysqli_query($conexion, "SELECT tmp.correlativo, tmp.codproducto, p.name, tmp.cantidad, tmp.precio_venta FROM detalle_temp tmp 
                                                    INNER JOIN frm_product p ON tmp.codproducto = p.id_product WHERE tmp.token_user = '$token'");
$result = mysqli_num_rows($query_detalle_temp);


$sub_total = 0;
$iva = 0;
$total = 0;
$arrayData = array();
if ($result > 0) {

    if ($result_iva > 0) {
        $info_iva = mysqli_fetch_assoc($query_iva);
        $iva = $info_iva['igv'];
    }
    while ($data = mysqli_fetch_assoc($query_detalle_temp)) {
        $precioTotal = round($data['cantidad'] * $data['precio_venta'], 2);
        $sub_total = round($sub_total + $precioTotal, 2);
        $total = round($total + $precioTotal, 2);

        $detalleTabla .= '<tr>
                <td>' . $data['codproducto'] . '</td>
                <td colspan="2">' . $data['name'] . '</td>
                <td class="textcenter">' . $data['cantidad'] . '</td>
                <td class="textright">' . $data['precio_venta'] . '</td>
                <td class="textright">' . $precioTotal . '</td>
                <td>
                    <a href="#" class="btn btn-danger" onclick="event.preventDefault(); del_product_detalle(' . $data['correlativo'] . ');"><i class="fas fa-trash-alt"></i> Eliminar</a>
                </td>
            </tr>';
    }
    $impuesto = round($sub_total / $iva, 2);
    $tl_sniva = round($sub_total - $impuesto, 2);
    $total = round($tl_sniva + $impuesto, 2);
    $detalleTotales = '<tr>
            <td colspan="5" class="textright">Sub_Total S/.</td>
            <td class="textright">' . $impuesto . '</td>
        </tr>
        <tr>
            <td colspan="5" class="textright">Igv (' . $iva . '%)</td>
            <td class="textright">' . $tl_sniva . '</td>
        </tr>
        <tr>
            <td colspan="5" class="textright">Total S/.</td>
            <td class="textright" id="totalvent">' . $total . '</td>
        </tr>';
    $arrayData['detalle'] = $detalleTabla;
    $arrayData['totales'] = $detalleTotales;
}

?>



<!-- Begin Page Content -->
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">



            <div class="row" style="position: relative; ">
                <div class="col-sm-4">

                    <div style="margin-left: 20px; width: 100px;">
                        <input type="checkbox" class="form-check-input" id="checkbox22">
                        <p id="infop2">Recibir data </p>


                    </div>
                    <nav>

                        <input type="text" class="form-control" name="bcbarra" id="barcodes" placeholder="Codigo de Barra..." style="background: #E8F8F5; color: #3498DB; ">

                        <form action="" id="form" method="post" style="margin-top: 15px;">

                            <input type="text" class="form-control form-control-user" name="busqueda" id="search" placeholder="Buscar por Nombre...">

                            <ul id="response" style="position: absolute; ">

                            </ul>






                        </form>

                    </nav>
                    <!-- Pending Requests Card Example -->



                </div>
                <div class="col-sm-8">

                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label><i class="fas fa-user"></i> Vendedor</label>
                                <p style="font-size: 16px; text-transform: uppercase; color: blue;"><?php echo $_SESSION['nombre']; ?></p>
                            </div>

                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <label> <i class="fa fa-inbox"></i> Estado caja </label>
                                    <?php
                                    $isActive = '';
                                    $ObjBox = new SalesController();
                                    $listB = $ObjBox->verifyBoxController();
                                    foreach ($listB as $b) {
                                        $isActive = $b[0];
                                    }
                                    if ($isActive == "1") { ?>
                                        <p id="box" style="font-size: 16px; text-transform: uppercase; color: #4EDCBC;">Abierto</p>
                                    <?php
                                    } else {
                                    ?>
                                        <p id="box" style="font-size: 16px; text-transform: uppercase; color: red;">Cerrado</p>
                                    <?php
                                    }

                                    ?>

                                </div>

                            </div>




                        </div>
                        <div class="col-lg-8">
                            <label>Acciones</label>
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="paga">Cuanto paga</label>
                                    <input type="number" step="any" placeholder="00.00" class="form-control" name="paga" id="paga" value="<?php echo $stock; ?>">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="cambio">Cambio</label>
                                    <input type="number" step="any" placeholder="00.00" class="form-control" name="cambio" id="cambio" value="<?php echo $stock; ?>">
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Tipo de Pago</label>
                                    <select name="tipopago" id="tipopago" class="form-control">
                                        <?php

                                        $ObjSales = new SalesController();
                                        $listS = $ObjSales->getTipePymentController();
                                        if ($listS  > 0) {
                                            foreach ($listS  as $s) {
                                        ?>
                                                <option value="<?php echo $s[0]; ?>"><?php echo $s[1] ?></option>
                                        <?php

                                            }
                                        }

                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div id="acciones_venta" class="form-group">
                                <a href="#" class="btn btn-danger" id="btn_anular_venta">Anular</a>
                                <a href="#" class="btn btn-primary" id="btn_facturar_venta"><i class="fas fa-save"></i> Generar Venta</a>
                                <input type="hidden" name="txt_cod_producto" id="txt_cod_producto"> 
                            </div>
                        </div>
                    </div>

                </div>
            </div>






            <h4 class="text-center">Datos Venta</h4>

            <div class="table-responsive">
                <table class="table table-hover">
                    <thead style="background: #00948B; color: white;">
                        <tr>
                            <th width="100px">Código Producto</th>
                            <th>Des.</th>
                            <th>Stock</th>
                            <th width="100px">Cantidad</th>
                            <th class="textright">Precio</th>
                            <th class="textright">Precio Total</th>
                            <th>Acciones</th>
                        </tr>
                        <tr style="background: white; color: black;">
                            <td><input type="number" name="txt_cod_producto2" id="txt_cod_producto2"  disabled></td>
                            <td id="txt_descripcion">-</td>
                            <td id="txt_existencia">-</td>
                            <td><input type="text" name="txt_cant_producto" id="txt_cant_producto" value="0" min="1"></td>
                            <td id="txt_precio" class="textright">00.00</td>
                            <td id="txt_precio_total" class="txtright">00.00</td>
                            <td><a href="#" id="add_product_venta" class="btn btn-dark" style="display: none;">Agregar</a></td>
                        </tr>
                        <tr>
                            <th>Id</th>
                            <th colspan="2">Descripción</th>
                            <th>Cantidad</th>
                            <th class="textright">Precio</th>
                            <th class="textright">Precio Total</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody id="detalle_venta">
                        <!-- Contenido ajax -->
                        <?php echo $detalleTabla; ?>

                    </tbody>

                    <tfoot id="detalle_totales">
                        <!-- Contenido ajax -->
                        <?php echo $detalleTotales; ?>
                    </tfoot>
                </table>

            </div>
        </div>
    </div>

</div>

<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->


<?php include_once "includes/footer.php"; ?>