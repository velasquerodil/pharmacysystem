<?php 
include_once "includes/header.php";

require_once "controller/ModuleController.php";
require_once "controller/UserController.php";
require_once "controller/RoleController.php";
if (!empty($_POST)) {
    $alert = "";
    if (empty($_POST['nombre']) || empty($_POST['descripcion']) ) {
        $alert = '<div class="alert alert-primary" role="alert">
                    Todo los campos son obligatorios
                </div>';
    } else {

        $nombre = $_POST['nombre'];
        $descripcion = $_POST['descripcion'];
    
        $checkM1 =      !empty($_POST['defaultCheck1'])      ? $_POST['defaultCheck1']      : NULL;
        $checkM2 =      !empty($_POST['defaultCheck2'])      ? $_POST['defaultCheck2']      : NULL;
        $checkM3 =      !empty($_POST['defaultCheck3'])      ? $_POST['defaultCheck3']      : NULL;
        $checkM4 =      !empty($_POST['defaultCheck4'])      ? $_POST['defaultCheck4']      : NULL;
        $checkM5 =      !empty($_POST['defaultCheck5'])      ? $_POST['defaultCheck5']      : NULL;
        $checkM6 =      !empty($_POST['defaultCheck6'])      ? $_POST['defaultCheck6']      : NULL;
        $checkM7 =      !empty($_POST['defaultCheck7'])      ? $_POST['defaultCheck7']      : NULL;

            /**save rol**/

        $rolOb= new RoleController();
        $existRol=$rolOb->verifyRolController($nombre);
        if(!empty($existRol)){
            $alert = '<div class="alert alert-danger" role="alert">
                        Rol ya existe
                    </div>';

        }else{

            $query_insert = $rolOb->saveRolController($nombre,$descripcion,1);
            if($query_insert){
                $alert = '<div class="alert alert-primary" role="alert" style="background: #4EDCBC;">
                Rol registrado
                </div>';

            }

            /****************get last  id_rol**************/
            $roleCon = new RoleController();
            $listRol = $roleCon->getlastRolController();
            $id_rol=0;
            foreach($listRol as $rl){
                $id_rol=$rl[0];
            }

            /*************Save Access modules **************/
            $saveAccess= new ModuleController();
            $modules = array(1,2,3,4,5,6,7);//modules 
            $tam = count($modules);
            for($i=0; $i<$tam; $i++){
                if($modules[$i]!=NULL){
                    //echo $modules[$i];
                    $saveAccess->saveAccesModuleController($id_rol,1,$modules[$i],0);
                }
            }

            /****************actualizar modulos seleccionados**************/
            $idmodule = array($checkM1,$checkM2,$checkM3,$checkM4,$checkM5,$checkM6,$checkM7);
            $lenht = count($idmodule);
            for($i=0; $i<$lenht; $i++){
                if($idmodule[$i]!=NULL){
                    //echo $idmodule[$i];
                    $saveAccess->editAccesModuleController(1,$idmodule[$i],$id_rol);
                }
            }
        }
    }
}
?>

<!--Begin Page Content-->
<div class="container-fluid">
        <!--<a href="lista_usuarios.php" class="btn btn-primary">Regresar</a>-->
    <!-- Content Row -->

    <div class="card shadow mb-4">
            <div class="card-header py-3">
                    <div class="d-sm-flex align-items-center justify-content-between">
                    <h5 class="m-0 font-weight-bold text-primary">Registrar Nuevo Rol</h5>
                        <a href="list_roles.php" class="btn btn-primary">Regresar</a>
                    </div>
            </div>
            
        <div class="card-body">

            <div class="row">
                <div class="col-lg-8 m-auto">
                    <form action="" method="post" autocomplete="off">
                        <?php echo isset($alert) ? $alert : ''; ?>
                        <div class="form-group">
                            <label for="nombre">Nombre</label>
                            <input type="text" class="form-control" placeholder="Ingrese Nombre" name="nombre" id="nombre" required>
                        </div>

                        <div class="form-group">
                            <label for="descripcion">Decripcion</label>
                            <input type="text" class="form-control" placeholder="Ingrese Descripcion" name="descripcion" id="descripcion">
                        </div>

                        <div class="d-sm-flex align-items-center justify-content-between mb-4">
                            <h3 class="h3 mb-0 text-gray-800" style="margin-top: 20px;">Acceso de Modulos</h3>
                        
                        </div>

                        <div class="row">
                            <!-- Earnings (Monthly) Card Example -->
                            
                            <?php

                                
                                $objalu = new ModuleController();
                                $listar = $objalu->listModuleController($_SESSION['rol']);	
                                $count=1;
                                foreach ($listar as $module){
                                    //echo "module".$count.": ".$module[3]."\n";
                                
                                ?>
                                    <a class="col-xl-3 col-md-6 mb-4" >
                                        <div class="card border-left-success shadow h-100 py-2">
                                            <div class="card-body">
                                                <div class="row no-gutters align-items-center">
                                                    <div class="col mr-2">
                                                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1"><?php echo $module[5]?></div>

                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" name="defaultCheck<?php echo $count?>" id="defaultCheck<?php echo $count?>"  value="<?php echo $module[3]?>"   >
                                                            <label class="custom-control-label" for="defaultCheck<?php echo $count?>">Seleccione</label>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </a>

                                <?php 
                                $count++;  }?>

                        </div>

                        <input type="submit" value="Guardar Rol" class="btn btn-primary">
                    </form>
                </div>
            </div>                        
        </div>
    </div>
</div>


   



</div>
<!-- End of Main Content -->
<?php include_once "includes/footer.php"; ?>