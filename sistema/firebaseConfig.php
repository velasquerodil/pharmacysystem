<?php
   require __DIR__.'\vendor\autoload.php';

   use Kreait\Firebase\Factory;
   use Kreait\Firebase\ServiceAccount;

   // This assumes that you have placed the Firebase credentials in the same directory
   // as this PHP file.
   $factory = (new Factory)->withServiceAccount(__DIR__.'\model\firebase_config\boticanievesscan-firebase-adminsdk-zv8gk-8eb342a975.json')
               ->withDatabaseUri('https://boticanievesscan-default-rtdb.firebaseio.com');
   $database = $factory->createDatabase();

?>