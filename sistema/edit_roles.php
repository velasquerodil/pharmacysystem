<?php
include "includes/header.php";
require_once "controller/RoleController.php";
require_once "controller/ModuleController.php";

if (!empty($_POST)) {
  $alert = "";
  if (empty($_POST['nombre']) || empty($_POST['descripcion']) ) {
    $alert = '<p class"error">Todo los campos son requeridos</p>';
  } else {
    $idRol = $_GET['id'];
    $nombre = $_POST['nombre'];
    $descripcion = $_POST['descripcion'];

    $checkM1 =      !empty($_POST['defaultCheck1'])      ? $_POST['defaultCheck1']      : NULL;
    $checkM2 =      !empty($_POST['defaultCheck2'])      ? $_POST['defaultCheck2']      : NULL;
    $checkM3 =      !empty($_POST['defaultCheck3'])      ? $_POST['defaultCheck3']      : NULL;
    $checkM4 =      !empty($_POST['defaultCheck4'])      ? $_POST['defaultCheck4']      : NULL;
    $checkM5 =      !empty($_POST['defaultCheck5'])      ? $_POST['defaultCheck5']      : NULL;
    $checkM6 =      !empty($_POST['defaultCheck6'])      ? $_POST['defaultCheck6']      : NULL;
    $checkM7 =      !empty($_POST['defaultCheck7'])      ? $_POST['defaultCheck7']      : NULL;


    $Rolobj = new RoleController();
    $Rolobj->editRolController($nombre,$descripcion,1,$idRol);

    /****************actualizar modulos seleccionados**************/
    $saveAccess= new ModuleController();
    $idmodule = array($checkM1,$checkM2,$checkM3,$checkM4,$checkM5,$checkM6,$checkM7);
    $lenht = count($idmodule);
    for($i=0; $i<$lenht; $i++){
        if($idmodule[$i]!=NULL){
            $saveAccess->editAccesModuleController(1,$idmodule[$i],$_GET['id']);
        }
    }
    $id=1;
    for($i=0; $i<$lenht; $i++){
        if(empty($idmodule[$i])){
            $saveAccess->editAccesModuleController(0,$id,$_GET['id']);
        }
        $id++;
    }

    $alert = '<p>Rol Actualizado</p>';
  }
}

// Mostrar Datos

$idRolobj = new RoleController();
$rol=$idRolobj->getRolController($_GET['id']);

$nombre = "";
$description = "";
$active = "";

if ($rol > 0) {
    foreach($rol as $rl){
        $nombre = $rl[1];
        $description = $rl[2];
        $active = $rl[3];
    }
}

?>


<!--Begin Page Content-->
<div class="container-fluid">
    <!-- Page Heading -->


    <div class="card shadow mb-4">
            <div class="card-header py-3">
                    <div class="d-sm-flex align-items-center justify-content-between">
                    <h5 class="m-0 font-weight-bold text-primary">Registrar Nuevo Rol</h5>
                        <a href="list_roles.php" class="btn btn-primary">Regresar</a>
                    </div>
            </div>
        <div class="card-body">
            <!-- Content Row -->
            <div class="row">
                <div class="col-lg-8 m-auto">
                    <form action="" method="post" autocomplete="off">
                        <?php echo isset($alert) ? $alert : ''; ?>
                        <div class="form-group">
                            <label for="nombre">Nombre</label>
                            <input type="text" class="form-control" placeholder="Ingrese Nombre" name="nombre" id="nombre" value="<?php echo $nombre?>" required>
                        </div>

                        <div class="form-group">
                            <label for="descripcion">Decripcion</label>
                            <input type="text" class="form-control" placeholder="Ingrese Descripcion" name="descripcion" id="descripcion" value="<?php echo $description?>">
                        </div>

                        <div class="d-sm-flex align-items-center justify-content-between mb-4">
                            <h3 class="h3 mb-0 text-gray-800" style="margin-top: 20px;">Acceso de Modulos</h3>
                        
                        </div>

                        <div class="row">
                            <!-- Earnings (Monthly) Card Example -->
                            
                            <?php

                                
                                $objalu = new ModuleController();
                                $listar = $objalu->listModuleController($_SESSION['rol']);	
                                $activeModules = $objalu->listActiveModulesByRolController($_GET['id'],1);	
                                $count=1;
                                foreach ($listar as $module){
                                    //echo "module".$count.": ".$module[3]."\n";

                                
                                
                                ?>
                                    <a class="col-xl-3 col-md-6 mb-4" >
                                        <div class="card border-left-success shadow h-100 py-2">
                                            <div class="card-body">
                                                <div class="row no-gutters align-items-center">
                                                    <div class="col mr-2">
                                                    
                                                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1"> <?php echo $module[5]?></div>
 
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" name="defaultCheck<?php echo $count?>" id="defaultCheck<?php echo $count?>"  value="<?php echo $module[3]?>" 
                                                            <?php 
                                                            foreach($activeModules as $act){
                                                                if($module[5]==$act[5]){
                                                                    echo "checked";
                                                                }} ?>  >
                                                            <label class="custom-control-label" for="defaultCheck<?php echo $count?>">Seleccione</label>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </a>

                                <?php 
                                $count++;  }?>

                        </div>

                        <input type="submit" value="Actualizar Rol" class="btn btn-primary">

                    </form>

                </div>

            </div>
        </div>
    </div>
</div>

   



</div>
<!-- /.container-fluid -->


<!-- End of Main Content -->
<?php include_once "includes/footer.php"; ?>