<?php
require_once "controller/ClientController.php";
if (!empty($_POST)) {
  $alert = "";
  if (empty($_POST['nombre']) || empty($_POST['telefono']) || empty($_POST['direccion'])) {
    $alert = '<p class"error">Todo los campos son requeridos</p>';
  } else {
    $idcliente = $_POST['id'];
    $dni = $_POST['dni'];
    $nombre = $_POST['nombre'];
    $telefono = $_POST['telefono'];
    $direccion = $_POST['direccion'];

    $objalu = new ClientController();
	   
    $sql_update = $objalu->updateClientController($dni,$nombre,$telefono,$direccion,$idcliente);

    if ($sql_update) {
        $alert = '<p class"exito">Cliente Actualizado correctamente</p>';
      } else {
        $alert = '<p class"error">Error al Actualizar el Cliente</p>';
      }
    
  }
}
// Mostrar Datos

if (empty($_REQUEST['id'])) {
  header("Location: lista_cliente.php");
}
$idcliente = $_REQUEST['id'];
$objalu = new ClientController();
$sql = $objalu->findClientByIdController($idcliente);

$idcliente = "";
$dni2 = "";
$nombre = "";
$telefono = "";
$direccion = "";

if (is_iterable($sql) == 0) {
  header("Location: lista_cliente.php");
} else {
  foreach ($sql as $client) {
    $idcliente = $client[0];
    $$dni2 = $client[1];
    $nombre = $client[2];
    $telefono = $client[3];
    $direccion = $client[4];
  }
}
?>
<?php include_once "includes/header.php"; ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">


        
          <div class="row">
            <div class="col-lg-6 m-auto">

            <div class="card shadow mb-4">
              <div class="card-header py-3">
                        <div class="d-sm-flex align-items-center justify-content-between">
                        <h5 class="m-0 font-weight-bold text-primary">Editar Usuario</h5>
                            <a href="lista_cliente.php" class="btn btn-primary">Regresar</a>
                        </div>
                </div>

                <div class="card-body">

                    <form class="" action="" method="post">
                      <?php echo isset($alert) ? $alert : ''; ?>
                      <input type="hidden" name="id" value="<?php echo $idcliente; ?>">
                      <div class="form-group">
                        <label for="dni">Dni</label>
                        <input type="number" placeholder="Ingrese dni" name="dni" id="dni" class="form-control" value="<?php echo $$dni2; ?>" required>
                      </div>
                      <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input type="text" placeholder="Ingrese Nombre" name="nombre" class="form-control" id="nombre" value="<?php echo $nombre; ?>" required>
                      </div>
                      <div class="form-group">
                        <label for="telefono">Teléfono</label>
                        <input type="number" placeholder="Ingrese Teléfono" name="telefono" class="form-control" id="telefono" value="<?php echo $telefono; ?>">
                      </div>
                      <div class="form-group">
                        <label for="direccion">Dirección</label>
                        <input type="text" placeholder="Ingrese Direccion" name="direccion" class="form-control" id="direccion" value="<?php echo $direccion; ?>">
                      </div>
                      <button type="submit" class="btn btn-primary"><i class="fas fa-user-edit"></i> Editar Cliente</button>
                    </form>
                    </div>
            </div>
          </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
      <?php include_once "includes/footer.php"; ?>