<?php include_once "includes/header.php"; ?>

<!-- Begin Page Content -->
<div class="container-fluid">

	<!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800">Clientes</h1>
		<!-- <a href="registro_cliente.php" class="btn btn-primary">Nuevo</a> -->
		
	</div>

	<div class="card shadow mb-4">
		<div class="card-header py-3">
							<h6 class="m-0 font-weight-bold text-primary float-left">Lista de Clientes</h6>
							<a href="registro_cliente.php" class="float-right  btn btn-primary">Nuevo</a>
					
         </div>
		<div class="card-body">

			<div class="table-responsive">
				<table class="table table-striped table-bordered" id="table">
					<thead class="table" id="head">
						<tr style="font-size: 14px;">
							<th>Id</th>
							<th>Dni</th>
							<th>Nombre</th>
							<th>Teléfono</th>
							<th>Dirección</th>
							<?php if ($_SESSION['rol'] == 1) { ?>
							<th>Acciones</th>
							<?php } ?>
						</tr>
					</thead>
					<tbody>
					<?php
						require_once "controller/ClientController.php";
						$objalu = new ClientController();
						$listar = $objalu->listClientesController();			
						
						if ($listar > 0) {
							foreach ($listar as $fila) { ?>
								<tr style="background: white; font-size: 13px;">
									<td id="tr"><?php echo $fila[0]; ?></td>
									<td id="tr"><?php echo $fila[1]; ?></td>
									<td id="tr"><?php echo $fila[2]; ?></td>
									<td id="tr"><?php echo $fila[3]; ?></td>
									<td id="tr"><?php echo $fila[4]; ?></td>
									<?php if ($_SESSION['rol'] == 1) { ?>
									<td id="tr">
										<div class="row">
											<div style="display: inline-block;">

												<a href="editar_cliente.php?id=<?php echo $fila[0]; ?>" class="btn btn-success"><i class='fas fa-edit'></i></a>
											</div>
											<div style="display: inline-block; margin-left: 2px;">
												<form action="option/deleteClient.php?id=<?php echo $fila[0]; ?>" method="post" class="confirmar d-inline">
													<button class="btn btn-danger" type="submit"><i class='fas fa-trash-alt'></i> </button>
												</form>
											</div>	
									</td>
									<?php } ?>
								</tr>
						<?php }
						} ?>
					</tbody>

				</table>
			</div>

		</div>
	</div>


</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->


<?php include_once "includes/footer.php"; ?>